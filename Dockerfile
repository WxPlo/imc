FROM php:7.0.21

RUN docker-php-ext-install pdo_mysql

COPY . /

EXPOSE 8000


ENTRYPOINT ["php", "artisan", "serve", "--host=0.0.0.0"]