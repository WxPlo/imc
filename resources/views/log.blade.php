@extends('layouts.app')

@section('content')
<div class="contact-area">
    <div class="container">
        <div class="row">
            <div class="login-box col-md-6 col-xs-12">

              @if (isset($log)  && !empty($log))
               <div class="alert alert-danger">
                   {{ $log }}
               </div>
               @endif
               @if (isset($signup)  && !empty($signup))
                <div class="alert alert-success">
                    {{ $signup }}
                </div>
                @endif
                <div class="contact-content">
                    <h2>Log In</h2>
                    <p>To Your Account
                    </p>
                </div>


                <div class="login-form">
                    {{ Form::open(array('url' => '/log')) }}
                    <div class="input-row">
                        {{ Form::label("Email") }}
                        {{ Form::text('username', '', array('required' => 'required')) }}
                    </div>
                    <div class="input-row">
                        {{ Form::label("Password") }}
                        {{ Form::password('password', array('required' => 'required')) }}
                    </div>
                    <div class="input-row">
                          {{ Form::submit('Login', array('class' => 'send'))}}
                    </div>

                    {{ Form::close() }}

                </div>
            </div>
            <div class="signup-box col-md-6 col-xs-12">
                <div class="contact-content">
                    <h2>Sign Up</h2>
                    <p>Join the community!</p>
                </div>
                <div class="signup-form">
                {{ Form::open(array('url' => '/log')) }}
                <div class="input-row">
                    {{ Form::label("First name") }}
                    {{ Form::text('firstname', '', array('required' => 'required')) }}
                </div>
                <div class="input-row">
                    {{ Form::label("Name") }}
                    {{ Form::text('name', '', array('required' => 'required')) }}
                </div>
                <div class="input-row">
                    {{ Form::label("Gender") }}
                    {{ Form::select('sexe', ['Male', 'Female']) }}
                </div>
                <div class="input-row">
                    {{ Form::label("Age") }}
                    {{ Form::text('age', '', array('required' => 'required')) }}
                </div>
                <div class="input-row">
                    {{ Form::label("Country") }}
                    {{ Form::text('country', '', array('required' => 'required')) }}
                </div>
                <div class="input-row">
                    {{ Form::label("Email") }}
                    {{ Form::text('email', '', array('required' => 'required')) }}
                </div>
                <div class="input-row">
                    {{ Form::label("Password") }}
                    {{ Form::password('password', array('required' => 'required')) }}
                </div>
                <div class="input-row">
                    {{ Form::label("Password confirmation") }}
                    {{ Form::password('cpassword', array('required' => 'required')) }}
                </div>
                <div class="input-row">
                      {{ Form::submit('Sign up', array('class' => 'send'))}}
                </div>

                {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- contact-area-end -->
@endsection
