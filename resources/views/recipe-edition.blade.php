@extends('layouts.app')

@section('content')

    <!-- recipe-edition-area-start -->
    <!--<div class="recipe-form-area">-->
    <!-- @if(isset($store))
        STORE SET
        tagName  {{ $store->tags[0]['id']}}
                orderSteps  {{ $store->steps[0]['order']}}
                name  {{ $store->name}}
                desc  {{ $store->description}}
                id  {{ $store->user_id}}
                date {{ $store->creation_date }}


    @endif -->
    <?php $var = 0; ?>
    @if(!isset($recipe) || $recipe === null)
      <section class="ht-section hs-404">
          <div class="container">
              <div class="col-xs-12 col-sm-8 col-sm-offset-2 inner">
                  <div class="media"><img src="{{ asset('img/404.png') }}" alt=""></div>
                  <h2>Oops! That recipe can't be found</h2>
                  <p>Sorry, but the recipe you are looking for does not exists.</p>
                  <a href="{{ url('/') }}" class="ht-button view-more-button">
                      <i class="fa fa-arrow-left"></i> BACK TO HOME <i class="fa fa-arrow-right"></i>
                  </a>
              </div>
          </div>
      </section>
    @elseif(Cookie::get('client') !== null)
      @if(isset(Cookie::get('client')['id']) && (Cookie::get('client')['id'] === $recipe['user_id'] || Cookie::get('client')['role'] === 'admin'))
        <div class="ht-section hs-recipe submit">
            <div class="container">
                <div class="row">
                    <div class="recipe-form-box">
                        <div class="recipe-form-content">
                            <h2>Edit a recipe</h2>
                            <p>Let's make it better!</p>
                        </div>
                        <div class="recipe-form-form">

                        <!-- FORM START -->
                        {{ Form::open(array('url' => '/recipe/' . $recipe['id'] . '/update')) }}

                        <!-- HIDDEN FIELDS HERE -->
                            {{ Form::hidden('recipe_id', $recipe['id']) }}

                            <div class="form-group col-sm-6">
                                <p> {{ Form::label("Recipe title") }}<p>
                                {{ Form::text('name', $recipe['name'], array('required' => 'required')) }}

                                <p class="des">Keep it short and descriptive</p>
                            </div>

                            <div class="form-group col-sm-6">
                                <label>Recipe photo</label>
                                {{ Form::file('image', ['class' => 'sr-only', 'id' => 'photo-upload']) }}
                                <input type="file" id="photo-upload" class="sr-only">
                                <!-- Use for display and get file url only -->
                                <div class="upload" data-target="#photo-upload">
                                    <input type="file" accept="image/*">
                                </div>
                            </div>

                            <div class="form-group col-sm-4">
                                {{ Form::label("RECIPE CATEGORY") }}
                                {{ Form::select('tags', ['1' => 'summer',
                                '4' => 'autumn',
                                '2' => 'winter',
                                '3' => 'spring']) }}
                            </div>

                            <div class="form-group col-sm-4">
                                {{ Form::label("Type") }}
                                {{ Form::select('minigames', ['None', 'None']) }}
                            </div>

                            <div class="form-group col-sm-4">
                                <label>CUISINE</label>
                                <select>
                                    <option value="" disabled selected>Choose category</option>
                                    <option value="">Example</option>
                                    <option value="">Example</option>
                                </select>
                            </div>

                            <div class="form-group col-sm-12">
                                <p> {{ Form::label("Short description") }}<p>
                                {{ Form::textarea('description', $recipe['description'], ['size' => '30x10', 'required']) }}
                            </div>

                            <div class="form-group range-slider col-sm-6 hidden">
                                <label>pre time</label>
                                <input type="text" id="pre-time" value="00:00" />
                                <div class="slider-range" data-target="#pre-time" data-max="6"></div>
                                <p class="des">
                                    <span class="left">00:00</span><span class="right">06:00</span>
                                    <span class="time-format"><span>HH:MM</span></span>
                                </p>
                            </div>

                            <div class="form-group range-slider col-sm-6 hidden">
                                <label>cook time</label>
                                <input type="text" id="cook-time" value="00:00" />
                                <div class="slider-range" data-target="#cook-time" data-max="6"></div>
                                <p class="des">
                                    <span class="left">00:00</span><span class="right">06:00</span>
                                    <span class="time-format"><span>HH:MM</span></span>
                                </p>
                            </div>

                            <div class="form-group col-sm-12">
                                <label>Yields</label>
                                <input type="text">
                                <p class="des">ex. 4 Servings, 3 Cups, 6 Bowls, etc.</p>
                            </div>

                            <!-- <div class="form-group col-sm-12">
                                <label>Ingredients</label>
                                <div class="recipe-ingredient">
                                    <div class="row-recipe-ingredient">
                                        <div class="col-recipe-ingredient">
                                            <input type="text" class="form-control col-xs-6" placeholder="Ingredient" name="recipe_ingredient[]">
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-addnew-item"><i class="fa fa-plus-circle"></i> Add New Ingredient</button>
                                </div>
                            </div> -->

                            <div class="form-group col-sm-12">
                                <div class="post-about clearfix mar-bot-50">
                                    <div class="post-about-top">Steps</div>
                                    <div class="post-about-content">

                                        <label>Instructions</label>
                                        <div class="recipe-instructions">
                                            <!-- <div class="row-recipe-instructions">
                                                <div class="col-recipe-instructions">
                                                    <input type="text" class="form-control" placeholder="Instructions" name="recipe_instructions[]">
                                                </div>
                                            </div> -->

                                            <!-- STEPS ADDED HERE -->
                                            @if(isset($recipe['steps']) && sizeof($recipe['steps']) > 0)
                                            {{--*/ $var = 0; /*--}}
                                                @foreach($recipe['steps'] AS $step)

                                                <?php $var = 0; ?>
                                                    <div id="step">
                                                        <strong id="num" name="sn">Step {{ $step['order'] }}</strong>
                                                        <input type="hidden" name="lang" value="lang" />

                                                        {{ Form::label("Difficulty") }}
                                                        <select required name="{{ "steps[" . $step['order'] . "][difficulty]" }}">
                                                            <option value="{{ $step['difficulty'] }}" selected="selected">{{ $difficulties[$step['difficulty']] }}</option>
                                                            <option value="1">Poor</option>
                                                            <option value="2">Easy</option>
                                                            <option value="3">Reasonable</option>
                                                            <option value="4">Medium</option>
                                                            <option value="5">Good</option>
                                                            <option value="6">Knows what you do</option>
                                                            <option value="7">Tricky</option>
                                                            <option value="8">Hard</option>
                                                            <option value="9">Extreme</option>
                                                            <option value="10">Master Chef</option>
                                                        </select>

                                                        {{ Form::label("Ingredients") }}
                                                        <div id="ingred_container0">
                                                            @foreach($step['ingredients'] AS $ingredient)

                                                            {{ Form::hidden('ingredient_id', $step['id']) }}
                                                            <select required id='ingr' name="{{ "steps[" . $step['order'] . "][ingredients][".$var."][id]" }}">

                                                                    <option value="{{ $ingredient['id'] }}" selected="selected">{{ $ingredient['name'] }}</option>
                                                                @foreach($ingredients AS $ingredient)
                                                                    <option value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>
                                                                @endforeach
                                                            <!-- <input type="button" class="btn btn-addnew-item" id="delIng" value="X"
                                                                onclick="removeIng(this.parentNode);" /> -->
                                                            </select>
                                                            <?php $var = $var + 1; ?>

                                                            @endforeach
                                                        </div>
                                                        <button type="button" class="btn btn-addnew-item" onClick="addIngredient(this)"><i class="fa fa-plus-circle"></i> Add a new ingredient</button>


                                                        {{ Form::label("Duration") }}
                                                        {{ Form::text("steps[" . $step['order'] . "][duration]", $step['duration'], array('required')) }} MIN

                                                        {{ Form::label("Short description") }}
                                                        {{ Form::textarea("steps[" . $step['order'] . "][description]", $step['description'], ['size' => '30x5', 'required']) }}

                                                        {{ Form::hidden("steps[" . $step['order'] . "][order]", $step['order']) }}
                                                        {{ Form::hidden("steps[" . $step['order'] . "][id]", $step['id']) }}
                                                    </div>
                                                @endforeach
                                            @endif
                                            <span id="writenode" />
                                            <button type="button" class="btn btn-addnew-item" onClick="addStep()"><i class="fa fa-plus-circle"></i> Add a new Step</button>
                                            <button type="button" class="btn btn-remove-item" onClick="removeLastStep()"><i class="fa fa-minus-circle"></i> Remove the last Step</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group col-sm-12">
                                <label>Additional note</label>
                                <textarea rows="5"></textarea>
                                <p class="des">Add any other notes like recipe source, cooking hints, etc. This section will show up under the cooking directions.</p>
                            </div>

                            <!-- / column -->
                            <div class="input-row">
                                <input class="send" type="submit" value="Update a recipe">
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="step-default" style="display: none">
            <strong id="num" name="sn">Step</strong>
            <input type="hidden" name="lang" value="lang" />

            {{ Form::label("Difficulty") }}

            <select name="steps[0][difficulty]" required>
                <option value="1">Poor</option>
                <option value="2">Easy</option>
                <option value="3">Reasonable</option>
                <option value="4">Medium</option>
                <option value="5">Good</option>
                <option value="6">Knows what you do</option>
                <option value="7">Tricky</option>
                <option value="8">Hard</option>
                <option value="9">Extreme</option>
                <option value="10">Master Chef</option>
            </select>


            {{ Form::label("Ingredients") }}
            <div id="ingred_container0">
                <select required id='ingr' name="steps[0][ingredients][0][id]">
                    <option value="" selected="selected">Select</option>
                    @foreach($ingredients AS $ingredient)
                        <option value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>
                    @endforeach
                <!-- <input type="button" class="btn btn-addnew-item" id="delIng" value="X"
        		onclick="removeIng(this.parentNode);" /> -->
                </select>
            </div>
            <button type="button" class="btn btn-addnew-item" onClick="addIngredient(this)"><i class="fa fa-plus-circle"></i> Add a new ingredient</button>

            {{ Form::label("Duration") }}
            {{ Form::text('steps[0][duration]', '', array('required')) }} MIN

            {{ Form::label("Short description") }}
            {{ Form::textarea('steps[0][description]', null, ['size' => '30x5', 'required']) }}

            {{ Form::hidden('steps[0][order]', 1) }}
        </div>

        <br/>

        <script>
          var counter = 0;
          @if(isset($recipe['steps']) && sizeof($recipe['steps']) > 0)
            counter = {{ sizeof($recipe['steps']) }} + 1;
          @endif

          function addStep()
          {
            var number = "Step " + counter;

            // Find the element to be copied
            var newNode = document.getElementById('step-default').cloneNode(true);
            newNode.id = '';
            newNode.setAttribute("class", counter );
            newNode.style.display = 'block';
            var newField = newNode.childNodes;
            // Give all fields a unique value
            for (var i=0;i<newField.length;i++)
            {
              var theName = newField[i].name;
              var theId = newField[i].id;

              if (theId == "num")
                newField[i].innerHTML = number;
              if (theId == "ingred_container0")
                newField[i].childNodes[1].name = "steps[" + counter + "][ingredients][0][id]";
              if (theId == "ingred_container0")
                newField[i].id = "ingred_container" + counter;
              if (theName == "steps[0][difficulty]")
                newField[i].name = "steps[" + counter + "][difficulty]";
              if (theName == "steps[0][description]")
                newField[i].name = "steps[" + counter+ "][description]";
              if (theName == "steps[0][duration]")
                newField[i].name = "steps[" + counter+ "][duration]";
              if (theName == "steps[0][order]")
              {
                var tmpc = counter + 1;
                newField[i].name = "steps[" + counter+ "][order]";
                newField[i].value = tmpc;
              }
              if (newField[i].id == "del" && counter == 0)
              {
                newNode.removeChild(newField[i]);
              }
            }
            counter++;
            var insertHere = document.getElementById('writenode');
            insertHere.parentNode.insertBefore(newNode,insertHere);
          }

          function removeLastStep() {
            var insertHere = document.getElementById('writenode');
            insertHere.parentNode.removeChild(insertHere.previousElementSibling);
            counter--;
          }

          function addIngredient(div) {
            // Find the element to be copied
            var stepNum = div.parentNode.className;
            var container = document.getElementById('ingred_container'+ stepNum);
            var ingNum = container.childNodes.length - 2;
            var newNode = document.getElementById('ingr').cloneNode(true);
            var stepNum = div.parentNode.className;

            newNode.id = '';
            newNode.style.display = 'block';
            var newField = container.childNodes;
            newNode.name = "steps[" + stepNum + "][ingredients]["+ ingNum +"][id]";

            container.appendChild(newNode);
          }

          function removeIng(div) {
            var childnum = div.childNodes;
            for (var i=childnum.length;i<0;i--)
            {
              if (i > 0)
              {
                div.removeChild(childnum[i]);
              }
            }
          }
        </script>
      @else
        <section class="ht-section hs-404">
            <div class="container">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2 inner">
                    <div class="media"><img src="{{ asset('img/404.png') }}" alt=""></div>
                    <h2>Oops! Permission denied.</h2>
                    <p>Sorry, you don't have permissions to edit this recipe.</p>
                    <a href="{{ url('/') }}" class="ht-button view-more-button">
                        <i class="fa fa-arrow-left"></i> BACK TO HOME <i class="fa fa-arrow-right"></i>
                    </a>
                </div>
            </div>
        </section>
      @endif
    @else
      <section class="ht-section hs-404">
          <div class="container">
              <div class="col-xs-12 col-sm-8 col-sm-offset-2 inner">
                  <div class="media"><img src="{{ asset('img/404.png') }}" alt=""></div>
                  <h2>Oops! You're not connected</h2>
                  <p>Please log in to edit a recipe.</p>
                  <a href="{{ url('/log') }}" class="ht-button view-more-button">
                      <i class="fa fa-arrow-left"></i> LOG IN <i class="fa fa-arrow-right"></i>
                  </a>
              </div>
          </div>
      </section>
    @endif
    <!-- contact-area-end -->
@endsection
