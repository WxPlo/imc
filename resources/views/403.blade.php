@extends('layouts.app')

@section('content')
<section class="ht-section hs-404">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 inner">
            <div class="media"><img src="{{ asset('img/404.png') }}" alt=""></div>
            <h2>Oops! Permission denied.</h2>
            <p>Sorry, you don't have permissions to see this page or to do this action.</p>
            <a href="{{ url('/') }}" class="ht-button view-more-button">
                <i class="fa fa-arrow-left"></i> BACK TO HOME <i class="fa fa-arrow-right"></i>
            </a>
        </div>
    </div>
</section>
@endsection
