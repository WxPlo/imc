@extends('layouts.app')

@section('content')
    <!-- post-content-area-start -->


    <div class="banner-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="banner">
                        <div class="banner-img">
                            <img src="{{ asset('img/item.jpg') }}" alt="">
                        </div>
                        <div class="banner-content hidden-xs">
                            <div class="contents">
                                <div class="contents-box">
                                    @if (Cookie::get('client'))
                                        <h2>Welcome {{Cookie::get('client')['name']}}</h2>
                                    @else
                                        <h2>Login first to create a million recipes</h2>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="post-content-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="post-article-area">

                        <div class="grid-post-col2">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-col2 mar-bot-50">
                                        <div class="col2-img post-img">
                                            <a href="{{ route('recipes') }}">
                                                <img src="{{ asset('img/categories/all_recipes.jpg') }}" alt="">
                                                <span style="font-size: 20px;">All recipes</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-col2 mar-bot-50">
                                        <div class="col2-img post-img">
                                            @if (Cookie::get('client'))
                                            <a href="{{ url('/recipes/'.Cookie::get('client')->id) }}">
                                                <img src="{{ asset('img/categories/my_recipes.jpg') }}" alt="">
                                                <span style="font-size: 20px;">My recipes</span>
                                            </a>
                                            @else
                                            <a href="#">
                                                <img src="{{ asset('img/categories/my_recipes.jpg') }}" alt="">
                                                <span style="font-size: 20px;">My recipes (Logged users)</span>
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-col2 mar-bot-35">
                                        <div class="col2-img post-img">
                                            <a href="{{ route('spring') }}">
                                                <img src="{{ asset('img/categories/spring.jpg') }}" alt="">
                                                <span style="font-size: 20px;">Spring recipes</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-col2 mar-bot-35">
                                        <div class="col2-img post-img">
                                            <a href="{{ route('summer') }}">
                                                <img src="{{ asset('img/categories/summer.jpg') }}" alt="">
                                                <span style="font-size: 20px;">Summer recipes</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-col2 mar-bot-35">
                                        <div class="col2-img post-img">
                                            <a href="{{ route('autumn') }}">
                                                <img src="{{ asset('img/categories/autumn.jpg') }}" alt="">
                                                <span style="font-size: 20px;">Autumn recipes</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="single-col2 mar-bot-35">
                                        <div class="col2-img post-img">
                                            <a href="{{ route('winter') }}">
                                                <img src="{{ asset('img/categories/winter.jpg') }}" alt="">
                                                <span style="font-size: 20px;">Winter recipes</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
