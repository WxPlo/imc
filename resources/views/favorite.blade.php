@extends('layouts.app')

@section('content')
@if(Cookie::get('client') && $user_id == Cookie::get('client')->id)
    <div class="recipe-index-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="recipe-row recipe-row-1">
                        <div class="post-about clearfix mar-bot-50">
                            <div class="post-about-top">Favorite Recipes</div>
                            <div class="post-about-content">
                                <p>You can find all the recipes you've liked.</p>
                            </div>
                        </div>
                        @foreach($recipes AS $recipe)
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <div class="single-col3 mar-bot-35">
                                    <div class="col2-img post-img">
                                        <a href="{{ route('recipe', ['id' => $recipe['id']]) }}">
                                            <img src="{{ $recipe->imageUrl }}" alt="">
                                        </a>
                                        <div class="post-img-content">
                                            <h5>
                                              <span ><b>{{ $recipe['name'] }}</b> Posted by </span>
                                              @if(isset($recipe['user']))
                                                   <a href="{{ route('profile', ['id' => $recipe['user']['id']]) }}">{{ $recipe['user']['firstname'] . ' ' . $recipe['user']['name'] }}</a>
                                              @endif
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <section class="ht-section hs-404">
        <div class="container">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 inner">
                <div class="media"><img src="{{ asset('img/404.png') }}" alt=""></div>
                <h2>Oops! That's not your favorites!</h2>
                <p>Sorry, but you're not allowed to see favorite recipes of other users.</p>
                <a href="{{ url('/') }}" class="ht-button view-more-button">
                    <i class="fa fa-arrow-left"></i> BACK TO HOME <i class="fa fa-arrow-right"></i>
                </a>
            </div>
        </div>
    </section>
@endif
@endsection
