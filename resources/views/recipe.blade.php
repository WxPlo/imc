@extends('layouts.app')

@section('content')
@if (isset($error) && !empty($error))
  <div class="alert alert-danger" style="text-align: center;">
     {{ $error }}
  </div>
@endif
@if (isset($msg) && !empty($msg))
  <div class="alert alert-success" style="text-align: center;">
     {{ $msg }}
  </div>
@endif

<div class="post-content-area recipe">
    <div class="hs-header">
        <div class="container">
            <h2 class="heading">{{ $recipe['name'] }}</h2>
            <p class="entry-meta meta-author">
                <a href="{{ route('profile', ['id' => $recipe['user_id']]) }}">
                    @if(isset($recipe['user']))
                        Post by {{ $recipe['user']['firstname'] . ' ' . $recipe['user']['name'] }}
                    @endif
                </a>
            </p>
            <p class="entry-meta">
                @if(isset($recipe['creation_date']))
                    Creation: {{ $recipe['creation_date'] }}
                @endif
                <br/>
            </p>
            @if(Cookie::get('client') !== null)
                @if(isset(Cookie::get('client')['id']) && (Cookie::get('client')['id'] === $recipe['user_id'] || Cookie::get('client')['role'] === 'admin'))
                  <a href="{{ url('/recipe/' . $recipe['id'] . '/update') }}">Edit</a>
                  <div class="deletion">
                    {{ Form::open(array('url' => '/recipe/delete')) }}
                    {{ Form::hidden('recipe_id', $recipe['id']) }}
                    {{ Form::hidden('user_id', Cookie::get('client')->id) }}
                    {{ Form::button('Delete', array('type' => 'submit', 'class' => ''))}}
                  </div>
                @endif
            @endif
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="post-article-area">
                    <div class="classic-post-item">
                        <div class="timer-header">
                            <ul class="clearfix">
                                @if(isset($recipe['difficulty']))
                                <li>
                                    <span>DIFFICULTY</span>
                                    <span>{{ $recipe['difficulty'] . ' / 10' }}</span>
                                </li>
                                @endif
                                @if(isset($recipe['duration']))
                                    <li>
                                        <span>TOTAL TIME</span>
                                        <span class="clc-icon">
                                            @if($recipe['duration']['hour'] > 1)
                                                {{ $recipe['duration']['hour'] . ' hours ' }}
                                            @elseif($recipe['duration']['hour'] == 1)
                                                {{ $recipe['duration']['hour'] . ' hour ' }}
                                            @endif
                                            @if($recipe['duration']['minute'] > 1)
                                                {{ $recipe['duration']['minute'] . ' mins ' }}
                                            @elseif($recipe['duration']['minute'] == 1)
                                                {{ $recipe['duration']['minute'] . ' min ' }}
                                            @endif
                                            @if($recipe['duration']['second'] > 1)
                                                {{ $recipe['duration']['second'] . ' secs ' }}
                                            @elseif($recipe['duration']['second'] == 1)
                                                {{ $recipe['duration']['second'] . ' sec ' }}
                                            @endif
                                        </span>
                                    </li>
                                @endif
                                @if(isset($recipe['likes']))
                                    <li>
                                        <span>LIKES</span>
                                        <span>{{ sizeof($recipe['likes']) }}</span>
                                    </li>
                                @endif
                                @if(isset($recipe['servings']))
                                    <li>
                                        <span>SERVINGS</span>
                                        <span>{{ $recipe['servings'] }}</span>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <div class="recipe-meta clearfix">
                            <div class="recipe-meta-content">
                                @if(Cookie::get('client') !== null)
                                    @if (isset($recipe['like_message']))
                                        {{ $recipe['like_message'] }}
                                    @endif
                                    <div class="share col-xs-6">
                                        @if(isset($recipe['likes']) && in_array(array('recipe_id' => $recipe['id'], 'user_id' => Cookie::get('client')->id), (array) json_decode($recipe['likes'], true)))
                                            {{ Form::open(array('url' => '/recipe/' . $recipe['id'] . '/unlike')) }}
                                            {{ Form::hidden('recipe_id', $recipe['id']) }}
                                            {{ Form::hidden('user_id', Cookie::get('client')->id) }}
                                            {{ Form::button('<i class="fa fa-heart"></i>', array('type' => 'submit', 'class' => ''))}}
                                            {{ Form::close() }}
                                        @else
                                            {{ Form::open(array('url' => '/recipe/' . $recipe['id'] . '/like')) }}
                                            {{ Form::hidden('recipe_id', $recipe['id']) }}
                                            {{ Form::hidden('user_id', Cookie::get('client')->id) }}
                                            {{ Form::button('<i class="fa fa-heart-o"></i>', array('type' => 'submit', 'class' => ''))}}
                                            {{ Form::close() }}
                                        @endif
                                    </div>
                                @endif
                                <div class="like-comment col-xs-6">
                                    <a href="#">
                                        @if(isset($recipe['likes']))
                                            {{ sizeof($recipe['likes']) }}
                                        @else
                                            0
                                        @endif
                                        <i class="fa fa-heart-o"></i>
                                    </a>
                                    <a href="#comments-area">
                                        @if(isset($recipe['comments']))
                                            {{ sizeof($recipe['comments']) }}
                                        @else
                                            0
                                        @endif
                                        <i class="fa fa-comment-o"></i>
                                    </a>
                                </div>
                                <div class="share col-xs-4 hidden">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="post-about clearfix">
                            <div class="post-about-top">Description</div>
                            <div class="post-about-content">
                                <p>{{ $recipe['description'] }}</p>
                            </div>
                        </div>
                        <div class="post-timer">
                            <div class="row">
                                <div class="col-xs-12 mar-bot-35">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="timer-desc">
                                            <h3>INGREDIENTS</h3>
                                            <ul>
                                                @foreach($ingredients AS $ingredient)
                                                    <li>
                                                        @if(!isset($ingredient['quantity']) || $ingredient['quantity'] === null)
                                                            1
                                                        @else
                                                            {{ $ingredient['quantity'] }}
                                                        @endif
                                                        {{ $ingredient['name'] }}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="timer-right">
                                            <img src="{{ $recipe['imageUrl'] }}" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="timer-instruction">
                                        <h3>INSTRUCTIONS</h3>
                                        <ul>
                                            @foreach($recipe['steps'] AS $step)
                                                <li>
                                                    {{ $step['order'] }}. {{ $step['description'] }}
                                                </li>
                                            @endforeach
                                        </ul>
                                        <ul class="tags">
                                            @foreach($recipe['tags'] AS $tag)
                                                <li><a href="ingredients.html">{{ $tag['name'] }}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (isset($recommended_recipes))
                        <div class="also-like-area">
                            <h4>You may also like ...</h4>
                            <div class="row">
                                @foreach($recommended_recipes AS $r)
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <div class="single-col3 mar-bot-35">
                                            <div class="col2-img post-img">
                                                <a href="{{ route('recipe', ['id' => $r['id']]) }}">
                                                    <img src="{{ $r['imageUrl'] }}" alt="">
                                                </a>
                                                <div class="post-img-content">
                                                    <h5>
                                                        <span>{{ $r['name'] }}</span><br/>
                                                        @if(isset($r['user.name']) && isset($r['user.firstname']))
                                                            <a href="{{ route('profile', ['id' => $r['user.id']]) }}">{{ 'Post by ' . $r['user.firstname'] . ' ' . $r['user.name'] }}</a>
                                                        @endif
                                                    </h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    <div class="comments-area" id="comments-area">
                        <h4>COMMENTS</h4>
                        @foreach($recipe['comments'] AS $comment)
                            <div class="single-comment comment-1">
                                <div class="img-content clearfix">
                                    @if(isset($comment['user']))
                                        <div class="comment-img">
                                            <a href="{{ route('profile', ['id' => $comment['user']['id']]) }}"><img src="{{ $comment['user']['pictureUrl'] }}" alt=""></a>
                                        </div>
                                        <div class="comment-content">
                                                <a class="name" href="{{ route('profile', ['id' => $comment['user']['id']]) }}">
                                                    {{ $comment['user']['firstname'] . ' ' . $comment['user']['name'] }}
                                                </a>
                                            @if(isset($comment['date']) && isset($comment['time']))
                                                <span>{{ $comment['date'] . ' at ' . $comment['time'] }}</span>
                                            @endif
                                        </div>
                                    @endif
                                </div>
                                <div class="comment-p-reply">
                                    <p>{{ $comment['data'] }}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @if(Cookie::get('client') !== null)
                    <div class="leave-reply">
                        <h3>LEAVE A COMMENT</h3>
                        {{ Form::open(array('url' => '/recipe/comment')) }}
                        {{ Form::hidden('recipe_id', $recipe['id']) }}
                        @if(isset(Cookie::get('client')['id']))
                          {{ Form::hidden('user_id', Cookie::get('client')['id']) }}
                        @endif
                        {{ Form::label('Comment') }}
                        {{ Form::textarea('data') }}
                        <div class="comment-submit">
                            {{ Form::submit('Post Comment', array('class' => 'send'))}}
                        </div>
                        <div class="rate-this hidden">
                            <span>Rate this recipe</span>
                            <a href="#"><i class="fa fa-star-o"></i></a>
                            <a href="#"><i class="fa fa-star-o"></i></a>
                            <a href="#"><i class="fa fa-star-o"></i></a>
                            <a href="#"><i class="fa fa-star-o"></i></a>
                            <a href="#"><i class="fa fa-star-o"></i></a>
                        </div>
                        {{ Form::close() }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- post-content-area-end -->
@endsection
