<html>
    <body>
        <h1>Hello</h1>

        <?php
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
              echo "POST";
            }
            echo $_SERVER['REQUEST_METHOD'] ;
            //echo $status;
         ?>


         @if (isset($status)  && !empty($status))
          <div class="alert alert-success">
              {{ $status }}
          </div>
        @endif

        {{ Form::open(array('action' => 'UserController@login')) }}
            {{ Form::text('username') }}
            {{ Form::text('password') }}
            {{ Form::submit('Save', array('class' => 'btn'))}}
        {{ Form::close() }}

        <script src="{{ asset('js/jquery-1.12.4.min.js') }}"></script>
        <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('js/countdown.js') }}"></script>
        <script src="{{ asset('js/jquery.meanmenu.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/theme.js') }}"></script>
    </body>
</html>
