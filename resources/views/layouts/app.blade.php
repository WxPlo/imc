<!DOCTYPE html>
<html lang="en">

<head>
    <!--- Basic Page Needs  -->
    <meta charset="utf-8">
    <title>One Million Cooking</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Mobile Specific Meta  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/meanmenu.css') }}">
    <link rel="stylesheet" href="{{ asset('css/default.css') }}">
    <link rel="stylesheet" href="{{ asset('css/typography.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="img/favicon.ico">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="home-grid2-full" data-spy="scroll" data-target="#scroll-menu" data-offset="100">
<div id="preloader"></div>
<!-- header-start -->
<header id="home">
    <!-- mobile-menu-start -->
    <div class="mobile-menu-area visible-xs visible-sm">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mobile_menu">
                        <nav id="mobile_menu_active">
                          @if (Cookie::get('client'))
                            <ul>
                                <li><a href="{{ route('search') }}">SEARCH</a></li>
                                <li><a href="{{ url('/favorites/'.Cookie::get('client')->id) }}">FAVORITES</a></li>
                                <li><a href="{{ url('/recipe/create') }}">CREATE A RECIPE</a></li>
                                <li class="dropicon"><a href="{{ route('recipes') }}">RECIPES</a>
                                    <ul class="dropdown recipes">
                                        <li><a href="{{ route('recipes') }}">All recipes</a></li>
                                        <li><a href="{{ url('/recipes/'.Cookie::get('client')->id) }}">My recipes</a></li>
                                        <li><a href="{{ route('winter') }}">Winter recipes</a></li>
                                        <li><a href="{{ route('spring') }}">Spring recipes</a></li>
                                        <li><a href="{{ route('summer') }}">Summer recipes</a></li>
                                        <li><a href="{{ route('autumn') }}">Autumn recipes</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ url('/profile/'.Cookie::get('client')->id) }}">PROFILE</a></li>
                                <li><a href="{{ url('/logout') }}">LOGOUT</a>
                            </ul>
                            @else
                            <ul>
                                <li><a href="{{ url('/log') }}">LOGIN & SIGN UP</a>
                            </ul>
                            @endif
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="header-middle" style="background-image: url({{ asset('img/banner.jpg') }}); height: 400px; background-size: cover;">
    <!-- <div class="header-middle"> -->
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-3 hidden-xs">
                    <!-- <div id="rdn" class="subscribe animated flash">
                        <button id="rdnBut"class="send input-row" href="{{ url('/log') }}">SUBCRIBE</button>
                    </div> -->
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="logo">
                        <a href="{{ url('/') }}"><img src="{{ asset('img/Title.png') }}" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- menu-start -->
    <div class="menu-area hidden-sm hidden-xs" id="sticker">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="menu">
                        <nav>
                            @if (Cookie::get('client'))
                            <ul>
                                <li><a href="{{ route('search') }}">SEARCH</a></li>
                                <li><a href="{{ url('/favorites/'.Cookie::get('client')->id) }}">FAVORITES</a></li>
                                <li><a href="{{ url('/recipe/create') }}">CREATE A RECIPE</a></li>
                                <li class="dropicon"><a href="{{ route('recipes') }}">RECIPES</a>
                                    <ul class="dropdown recipes">
                                        <li><a href="{{ route('recipes') }}">All recipes</a></li>
                                        <li><a href="{{ url('/recipes/'.Cookie::get('client')->id) }}">My recipes</a></li>
                                        <li><a href="{{ route('winter') }}">Winter recipes</a></li>
                                        <li><a href="{{ route('spring') }}">Spring recipes</a></li>
                                        <li><a href="{{ route('summer') }}">Summer recipes</a></li>
                                        <li><a href="{{ route('autumn') }}">Autumn recipes</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ url('/profile/'.Cookie::get('client')->id) }}">PROFILE</a></li>
                                <li><a href="{{ url('/logout') }}">LOGOUT</a>
                            </ul>
                            @else
                            <ul>
                                <li><a href="{{ url('/log') }}">LOGIN & SIGN UP</a>
                            </ul>
                            @endif

                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- menu-end -->
</header>
<!-- header-end -->

@yield('content')

<!-- footer-start -->
<footer>
    <div class="footer-bottom">
        <div class="container">
            <div class="footer-bottom-area">
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="footer-copyright">
                            <p class="copyright">@ 2017 OneMillionCooking</p>
                        </div>
                    </div>
                    <!-- <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="footer-social follow-box">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                            <a href="#"><i class="fa fa-youtube-play"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-rss"></i></a>
                        </div>
                    </div> -->
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="footer-right-copy">
                            <a class="top-scroll smoothscroll" href="#home">TOP<span class="top-endi"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer-end -->
<!-- Scripts -->
<script src="{{ asset('js/jquery-1.12.4.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/countdown.js') }}"></script>
<script src="{{ asset('js/jquery.meanmenu.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/theme.js') }}"></script>
</body>

</html>
