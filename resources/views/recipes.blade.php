@extends('layouts.app')

@section('content')

@if (isset($error) && !empty($error))
  <div class="alert alert-danger" style="text-align: center;">
     {{ $error }}
  </div>
@endif
@if (isset($msg) && !empty($msg))
  <div class="alert alert-success" style="text-align: center;">
     {{ $msg }}
  </div>
@endif

    <div class="recipe-index-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="recipe-row recipe-row-1">
                        <div class="recipe-header">
                            @if(isset($user_id))
                                @if(Cookie::get('client') && $user_id == Cookie::get('client')->id)
                                    <div class="post-about clearfix mar-bot-50">
                                        <div class="post-about-top">My Recipes</div>
                                        <div class="post-about-content">
                                            @if($recipes !== null && count($recipes) != 0)
                                                <p>You can find all the recipes you've created.</p>
                                            @else
                                                <p>You have no recipes to show.</p>
                                            @endif
                                        </div>
                                    </div>
                                @elseif(isset($user) && isset($user['firstname']) && isset($user['name']))
                                    <div class="post-about clearfix mar-bot-50">
                                        <div class="post-about-top">{{ $user['firstname'] . ' ' . $user['name'] }}'s recipes</div>
                                        <div class="post-about-content">
                                            @if($recipes !== null && count($recipes) != 0)
                                                <p>You can find all the recipes {{ $user['firstname'] . ' ' . $user['name'] }} has created.</p>
                                            @else
                                                <p>{{ $user['firstname'] . ' ' . $user['name'] }} doesn't have recipes to show.</p>
                                            @endif
                                        </div>
                                    </div>
                                @else
                                    <section class="ht-section hs-404">
                                        <div class="container">
                                            <div class="col-xs-12 col-sm-8 col-sm-offset-2 inner">
                                                <div class="media"><img src="{{ asset('img/404.png') }}" alt=""></div>
                                                <h2>Oops! User not found!</h2>
                                                <p>Sorry, but this user doesn't exist.</p>
                                                <a href="{{ url('/') }}" class="ht-button view-more-button">
                                                    <i class="fa fa-arrow-left"></i> BACK TO HOME <i class="fa fa-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </section>
                                @endif
                            @else
                                <div class="post-about clearfix mar-bot-50">
                                    <div class="post-about-top">All recipes</div>
                                    <div class="post-about-content">
                                        @if($recipes !== null && count($recipes) != 0)
                                            <p>You can find all recipes.</p>
                                        @else
                                            <p>You have no recipes to show.</p>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </div>
                        @if ($recipes !== null)
                          @foreach ($recipes as $recipe)
                              <div class="col-sm-4 col-xs-12">
                                  <div class="mar-bot-35">
                                      <div class="post-img">
                                          <a href="{{ route('recipe', ['id' => $recipe['id']]) }}"><img src="{{$recipe->imageUrl}}" alt=""></a>
                                          <div class="post-img-content">
                                              <h5>
                                                  <span ><b>{{ $recipe['name'] }}</b> Posted by </span>
                                                  @if(isset($recipe['user']))
                                                      <a href="{{ route('profile', ['id' => $recipe['user']['id']]) }}">{{ $recipe['user']['firstname'] . ' ' . $recipe['user']['name'] }}</a>
                                                  @endif
                                              </h5>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
