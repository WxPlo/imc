@extends('layouts.app')

@section('content')

<div class="post-content-area">
    <div class="hs-header">
        <div class="container">
            <h2 class="heading">
                FOLLOWERS
            </h2>
        </div>
    </div>
    <div class="profile container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="post-article-area">
                  @if(Cookie::get('client') !== null)
                    @if(isset($user['description']))
                        <div class="post-about clearfix">
                            <div class="post-about-top">Description</div>
                            <div class="post-about-img">
                                <img src="../../public/img/banner/post-about.jpg" alt="">
                            </div>
                            <div class="post-about-content">
                                <p>{{ $user['description'] }}</p>
                            </div>
                        </div>
                    @endif
                    <div class="post-about clearfix">
                        <div class="post-about-top">Informations</div>
                        <div class="post-about-content">
                          <p><label for="firstname">Firstname: </label><span id="email"> {{ $user['firstname'] }}</span></p>
                          <p><label for="name">Name: </label><span id="email"> {{ $user['name'] }}</span></p>
                          <p><label for="country">Country: </label><span id="email"> {{ $user['country'] }}</span></p>
                          <p><label for="age">Age: </label><span id="email"> {{ $user['age'] }}</span></p>
                          <p><label for="sexe">Gender: </label><span id="email"> {{ $user['sexe'] }}</span></p>
                          <p><label for="email">Email: </label><span id="email"> {{ $user['email'] }}</span></p>
                        </div>
                    </div>
                    <div class="classic-post-item mar-bot-50">
                        <div class="post-meta">
                            <div class="follow">
                                @if(isset($user['followings']))
                                  <a href="#" class="item"> {{ sizeof($user['followings']) }} followings</i></a>
                                @endif
                                @if(isset($user['followers']))
                                  <a href="#" class="item"> {{ sizeof($user['followers']) }} followers</i></a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="followers">
                        @if (isset($user['followers']))
                          @foreach ($user['followers'] AS $follower)
                            <div class="single-comment comment-1">
                                <div class="img-content clearfix">
                                    <a href="{{ route('profile', ['id' => $follower['follower_id']]) }}">
                                      <div class="comment-img" style="margin: 10px;">
                                            @if(isset($follower['pictureUrl']) && $follower['pictureUrl'] !== null)
                                              <img src="{{ $follower['pictureUrl'] }}" alt="" style="width: 50px; height: 50px;">
                                            @endif
                                      </div>
                                      {{ $follower['user.name'] . ' ' . $follower['user.firstname'] }}
                                    </a>
                                </div>
                             </div>
                          @endforeach
                        @else
                          <span>No followers</span>
                        @endif
                    </div>
                  @else
                      You should login first to access this section !
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
