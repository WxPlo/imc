@extends('layouts.app')

@section('content')

    <div class="recipe-index-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <p>You can find all the winter recipes.</p>
                    <div class="recipe-row recipe-row-1">
                        <div class="post-about clearfix mar-bot-50">
                            <div class="post-about-top">Winter Recipes</div>
                            <div class="post-about-content">

                                @foreach($recipes AS $recipe)
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <div class="single-col3 mar-bot-35">
                                            <div class="col2-img post-img">
                                                <a href="{{ route('recipe', ['id' => $recipe->id]) }}">
                                                    <img src="{{$recipe->imageUrl}}" alt="">
                                                </a>
                                                <div class="post-img-content">
                                                    <h5>
                                                        <span>{{ $recipe->name }}</span><br/>

                                                        <br/>
                                                        @if(isset($recipe->userName) && isset($recipe->userFirstname))
                                                            <a href="{{ route('profile', ['id' => $recipe->userId]) }}">
                                                            {{ $recipe->userName }}</a>
                                                        @endif
                                                    </h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
