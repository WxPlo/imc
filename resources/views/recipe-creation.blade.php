@extends('layouts.app')

@section('content')

@if (isset($error) && !empty($error))
  <div class="alert alert-danger" style="text-align: center;">
     {{ $error }}
  </div>
@endif
@if (isset($msg) && !empty($msg))
  <div class="alert alert-success" style="text-align: center;">
     {{ $msg }}
  </div>
@endif

<!-- recipe-creation-area-start -->
    <!--<div class="recipe-form-area">-->
@if (Cookie::get('client'))
    <div class="ht-section hs-recipe submit">
        <div class="container">
            <div class="row">
                <div class="recipe-form-box">
                    <div class="recipe-form-content">
                        <h2>Submit a recipe</h2>
                        <p>Let's share with the community!</p>
                    </div>
                    <div class="recipe-form-form">

                      <!-- FORM START -->
                      {{ Form::open(array('url' => '/recipe/store')) }}

                        <!-- HIDDEN FIELDS HERE -->
                        {{ Form::hidden('owner', Cookie::get('client')) }}
                        {{ Form::hidden('user_id', Cookie::get('client')->id) }}
                        {{ Form::hidden('creation_date', date('Y-m-d H:i:s')) }}

                        <div class="form-group col-sm-6">
                            <p> {{ Form::label("Recipe title") }}<p>
                            {{ Form::text('name', '', array('required' => 'required')) }}
                            <p class="des">Keep it short and descriptive</p>
                        </div>

                        <div class="form-group col-sm-6">
                            <label>Recipe photo</label>
                            {{ Form::file('image', ['class' => 'sr-only', 'id' => 'photo-upload']) }}
                            <input type="file" id="photo-upload" class="sr-only">
                            <!-- Use for display and get file url only -->
                            <div class="upload" data-target="#photo-upload">
                                <input type="file" accept="image/*">
                            </div>
                        </div>

                        <!-- <div class="form-group col-sm-12">
                            <label>Difficulty level</label>
                            <select>
                                <option value="">Easy</option>
                                <option value="">Hard</option>
                                <option value="">Master</option>
                            </select>
                            <p class="des">Keep it short and descriptive</p>
                        </div> -->

                        <div class="form-group col-sm-4">
                            {{ Form::label("RECIPE CATEGORY") }}
                            {{ Form::select('tags', ['1' => 'summer',
                            '4' => 'autumn',
                            '2' => 'winter',
                            '3' => 'spring']) }}
                        </div>

                        <!-- <div class="form-group col-sm-4">
                            {{ Form::label("Type") }}
                            {{ Form::select('minigames', ['None', 'None']) }}
                        </div> -->

                        <div class="form-group col-sm-4 hidden">
                            <label>CUISINE</label>
                            <select>
                                <option value="" disabled selected>Other</option>
                                <option value="">French</option>
                                <option value="">Italian</option>
                                <option value="">Indian</option>
                                <option value="">American</option>
                                <option value="">Vietnamese</option>
                                <option value="">African</option>
                                <option value="">German</option>
                            </select>
                        </div>

                        <div class="form-group col-sm-12">
                            <p> {{ Form::label("Short description") }}<p>
                            {{ Form::textarea('description', null, ['size' => '30x10', 'required']) }}
                        </div>

                        <div class="form-group range-slider col-sm-6 hidden">
                            <label>pre time</label>
                            <input type="text" id="pre-time" value="00:00" />
                            <div class="slider-range" data-target="#pre-time" data-max="6"></div>
                            <p class="des">
                                <span class="left">00:00</span><span class="right">06:00</span>
                                <span class="time-format"><span>HH:MM</span></span>
                            </p>
                        </div>

                        <div class="form-group range-slider col-sm-6 hidden">
                            <label>cook time</label>
                            <input type="text" id="cook-time" value="00:00" />
                            <div class="slider-range" data-target="#cook-time" data-max="6"></div>
                            <p class="des">
                                <span class="left">00:00</span><span class="right">06:00</span>
                                <span class="time-format"><span>HH:MM</span></span>
                            </p>
                        </div>

                        <div class="form-group col-sm-12 hidden">
                            <label>Yields</label>
                            <input type="text">
                            <p class="des">ex. 4 Servings, 3 Cups, 6 Bowls, etc.</p>
                        </div>

                        <!-- <div class="form-group col-sm-12">
                            <label>Ingredients</label>
                            <div class="recipe-ingredient">
                                <div class="row-recipe-ingredient">
                                    <div class="col-recipe-ingredient">
                                        <input type="text" class="form-control col-xs-6" placeholder="Ingredient" name="recipe_ingredient[]">
                                    </div>
                                </div>
                                <button type="button" class="btn btn-addnew-item"><i class="fa fa-plus-circle"></i> Add New Ingredient</button>
                            </div>
                        </div> -->

                        <div class="form-group col-sm-12">
                          <div class="post-about clearfix mar-bot-50">
                              <div class="post-about-top">Steps</div>
                              <div class="post-about-content">
                                <label>Instructions</label>
                                <div class="recipe-instructions">
                                    <span id="writenode"></span>
                                    <button type="button" class="btn btn-addnew-item" onClick="addStep()"><i class="fa fa-plus-circle"></i> Add a new Step</button>
                                    <button type="button" class="btn btn-remove-item" onClick="removeLastStep()"><i class="fa fa-minus-circle"></i> Remove the last Step</button>
                                </div>
                              </div>
                          </div>

                        </div>
                        <div class="form-group col-sm-12 hidden">
                            <label>Additional note</label>
                            <textarea rows="5"></textarea>
                            <p class="des">Add any other notes like recipe source, cooking hints, etc. This section will show up under the cooking directions.</p>
                        </div>

                        <!-- / column -->
                        <div class="input-row">
                            <!-- <input class="send" type="submit" value="Create a recipe"> -->
                            {{ Form::submit('Create a recipe', array('class' => 'send')) }}
                        </div>
                    {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="step" style="display: none">
    	<strong id="num" name="sn">Step</strong>
      <!-- <input type="button" class="btn btn-addnew-item" id="del" value="X"
    		onclick="this.parentNode.parentNode.removeChild(this.parentNode);" /> -->
    	<input type="hidden" name="lang" value="lang" />

      {{ Form::label("Difficulty") }}
      {{ Form::select('steps[0][difficulty]', ['1' => 'Poor',
       '2' => 'Easy', '3' => 'Reasonable', '4' => 'Medium', '5' => 'Good',
      '6' => 'Knows what you do', '7' => 'Tricky', '8' => 'Hard', '9' => 'Extreme', '10' => 'Master Chef']) }}

      {{ Form::label("Ingredients") }}
      <div id="ingred_container0">
        <select required id='ingr' name="steps[0][ingredients][0][id]">
      		<option value="" selected="selected">Select</option>
          @foreach($ingredients AS $ingredient)
            <option value="{{ $ingredient->id }}">{{ $ingredient->name }}</option>
          @endforeach
          <!-- <input type="button" class="btn btn-addnew-item" id="delIng" value="X"
        		onclick="removeIng(this.parentNode);" /> -->
        </select>
      </div>
      <button type="button" class="btn btn-addnew-item" onClick="addIngredient(this)"><i class="fa fa-plus-circle"></i> Add a new ingredient</button>

      {{ Form::label("Duration") }}
      {{ Form::text('steps[0][duration]', '', array('required')) }} MIN

      {{ Form::label("Short description") }}
      {{ Form::textarea('steps[0][description]', null, ['size' => '20x5', 'required']) }}

      {{ Form::hidden('steps[0][order]', 1) }}


    </div>
  <br/>
  @else
  <div class="banner-area">
      <div class="container">
          <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="banner">
                      <div class="banner-img">
                          <img src="{{ asset('img/item.jpg') }}" alt="">
                      </div>
                      <div class="banner-content hidden-xs">
                          <div class="contents">
                              <div class="contents-box">
                                  @if (Cookie::get('client'))
                                      <h2>Welcome {{Cookie::get('client')['name']}}</h2>
                                  @else
                                      <h2>Login first to create a million recipes</h2>
                                  @endif
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  @endif
    <script>
      var counter = 0;
      function addStep()
      {
        var number = "Step " + counter;

        // Find the element to be copied
        var newNode = document.getElementById('step').cloneNode(true);
        newNode.id = '';
        newNode.setAttribute("class", counter );
        newNode.style.display = 'block';
        var newField = newNode.childNodes;
        // Give all fields a unique value
        for (var i=0;i<newField.length;i++)
        {
          var theName = newField[i].name;
          var theId = newField[i].id;

          if (theId == "num")
            newField[i].innerHTML = number;
          if (theId == "ingred_container0")
            newField[i].childNodes[1].name = "steps[" + counter + "][ingredients][0][id]";
          if (theId == "ingred_container0")
        	newField[i].id = "ingred_container" + counter;
          if (theName == "steps[0][difficulty]")
            newField[i].name = "steps[" + counter + "][difficulty]";
          if (theName == "steps[0][description]")
        	newField[i].name = "steps[" + counter+ "][description]";
          if (theName == "steps[0][duration]")
            newField[i].name = "steps[" + counter+ "][duration]";
          if (theName == "steps[0][order]")
          {
            var tmpc = counter + 1;
            newField[i].name = "steps[" + counter+ "][order]";
            newField[i].value = tmpc;
          }
          if (newField[i].id == "del" && counter == 0)
          {
            newNode.removeChild(newField[i]);
          }
        }
        counter++;
         var insertHere = document.getElementById('writenode');
         insertHere.parentNode.insertBefore(newNode,insertHere);
        }

        function removeLastStep() {
          var insertHere = document.getElementById('writenode');
          insertHere.parentNode.removeChild(insertHere.previousElementSibling);
          counter--;
        }

        function addIngredient(div) {
        		// Find the element to be copied
            var stepNum = div.parentNode.className;
            var container = document.getElementById('ingred_container'+ stepNum);
            var ingNum = container.childNodes.length - 2;
        		var newNode = document.getElementById('ingr').cloneNode(true);
            var stepNum = div.parentNode.className;

        		newNode.id = '';
        		newNode.style.display = 'block';
        		var newField = container.childNodes;
        		newNode.name = "steps[" + stepNum + "][ingredients]["+ ingNum +"][id]";

        		container.appendChild(newNode);
        }

        function removeIng(div) {
          var childnum = div.childNodes;
          for (var i=childnum.length;i<0;i--)
          {
              if (i > 0)
              {
                div.removeChild(childnum[i]);
              }
          }
        }
    </script>

    <!-- contact-area-end -->
  @endsection
