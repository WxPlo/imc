@extends('layouts.app')

@section('content')
    <div class="recipe-index-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="search-recipes">
                        <div class="header">
                            <div class="post-about clearfix mar-bot-50">
                                <div class="post-about-top">Search recipes</div>
                                <div class="post-about-content">
                                  <div class="search">
                                  {{ Form::open(array('url' => '/search')) }}
                                     {{ Form::text('name') }}
                                     {{ Form::button("<i class='fa fa-search'></i>", array('class' => 'send', 'type' => 'submit')) }}
                                  </div>
                                  <div class="mar-bot-50"></div>
                                  @if (isset($ingredients) && sizeof($ingredients) > 0)
                                    <div class="ingredients">
                                      {{ Form::label("Ingredient") }}
                                      {{ Form::select('ingredient', $ingredients) }}
                                    </div>
                                  @endif
                                  @if (isset($tags) && sizeof($tags) > 0)
                                    <div class="tags">
                                      {{ Form::label("Tag") }}
                                      {{ Form::select('tag', $tags) }}
                                    </div>
                                  @endif
                                {{ Form::close() }}

                                    @if (isset($recipes))
                                       @if(count($recipes) != 0)
                                            <p>See the recipes you were looking for.</p>
                                        @else
                                            <p>Sorry, no recipes found.</p>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if (isset($recipes))
                            @foreach ($recipes as $recipe)
                                <div class="col-sm-4 col-xs-12">
                                    <div class="mar-bot-35">
                                        <div class="post-img">
                                            <a href="{{ route('recipe', ['id' => $recipe['id']]) }}"><img src="{{$recipe->imageUrl}}" alt=""></a>
                                            <div class="post-img-content">
                                                <h5>
                                                    <span ><b>{{ $recipe['name'] }}</b> Posted by </span>
                                                    @if(isset($recipe['user']))
                                                        <a href="{{ route('profile', ['id' => $recipe['user']['id']]) }}">{{ $recipe['user']['firstname'] . ' ' . $recipe['user']['name'] }}</a>
                                                    @endif
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                         @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
