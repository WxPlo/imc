@extends('layouts.app')

@section('content')

<div class="post-content-area">
    <div class="hs-header">
        <div class="container">
            <h2 class="heading">
                PROFILE
            </h2>
        </div>
    </div>
    <div class="profile container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="post-article-area">
                  @if (isset($not_updated)  && !empty($not_updated))
                   <div class="alert alert-danger">
                       {{ $not_updated }}
                   </div>
                  @endif
                  @if (isset($updated)  && !empty($updated))
                    <div class="alert alert-success">
                        {{ $updated }}
                    </div>
                  @endif
                  @if (Cookie::get('client'))
                    @if(isset($user['description']))
                        <div class="post-about clearfix">
                            <div class="post-about-top">Description</div>
                            <div class="post-about-img">
                                <img src="../../public/img/banner/post-about.jpg" alt="">
                            </div>
                            <div class="post-about-content">
                                <p>{{ $user['description'] }}</p>
                            </div>
                        </div>
                    @endif
                    <div class="post-about clearfix">
                        <div class="post-about-top">Informations</div>
                        <div class="post-about-content">
                        @if(isset(Cookie::get('client')['id']) && (Cookie::get('client')['id'] === $user['id'] || Cookie::get('client')['role'] === 'admin'))
                          {{ Form::open(array('url' => '/profile/'.$user->id)) }}

                          @if(isset($user['id']))
                              {{ Form::hidden('id', $user['id']) }}
                          @endif
                          @if(isset($user['firstname']))
                              <p> {{ Form::label("Firstname") }}<p>
                              {{ Form::text('firstname', $user->firstname) }}
                          @endif
                          @if(isset($user['name']))
                              <p>  {{ Form::label("Name") }}<p>
                              {{ Form::text('name', $user->name) }}
                          @endif
                          @if(isset($user['country']))
                              <p> {{ Form::label("Country") }}<p>
                              {{ Form::text('country', $user->country) }}
                          @endif
                          @if(isset($user['age']))
                              <p> {{ Form::label("Age") }}</p>
                              {{ Form::text('age', $user->age) }}
                          @endif
                          @if(isset($user['sexe']))
                              <p> {{ Form::label("Gender") }}</p>
                              {{ Form::hidden('sexe', $user['sexe']) }}
                              {{ Form::text('sexe', ($user->sexe == 1) ? 'Female' : 'Male', array('disabled')) }}
                          @endif
                          @if(isset($user['email']))
                              <p> {{ Form::label("Email") }}</p>
                              {{ Form::hidden('email', $user['email']) }}
                              {{ Form::text('email', $user->email, array('disabled')) }}
                          @endif
                          <br/>
                          <br>
                          <div class="input-row">
                              <p>  {{ Form::submit('Update', array('class' => 'send'))}}</p>
                          </div>
                          {{ Form::close() }}
                        @else
                          <p><label for="firstname">Firstname: </label><span id="email"> {{ $user['firstname'] }}</span></p>
                          <p><label for="name">Name: </label><span id="email"> {{ $user['name'] }}</span></p>
                          <p><label for="country">Country: </label><span id="email"> {{ $user['country'] }}</span></p>
                          <p><label for="age">Age: </label><span id="email"> {{ $user['age'] }}</span></p>
                          <p><label for="sexe">Gender: </label><span id="email"> {{ $user['sexe'] }}</span></p>
                          <p><label for="email">Email: </label><span id="email"> {{ $user['email'] }}</span></p>
                        @endif
                        </div>
                    </div>
                    <div class="classic-post-item">
                        <div class="post-meta">
                            <div class="follow">
                                @if(isset($user['followings']))
                                    <a href="#" class="item"> {{ $user['followings'] }} followings</i></a>
                                @endif
                                @if(isset($user['followers']))
                                    <a href="#" class="item"> {{ $user['followers'] }} followers</i></a>
                                @endif
                            </div>
                            <div class="auth-social">
                                @if(isset($user['facebook']))
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                @endif
                                @if(isset($user['twitter']))
                                        <a href="#"><i class="fa fa-twitter"></i></a>
                                @endif
                                @if(isset($user['instagram']))
                                        <a href="#"><i class="fa fa-instagram"></i></a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="recipes">
                        <h4>Recipes</h4>
                        @foreach ($user['recipes'] AS $recipe)
                            <div class="col-sm-4 col-xs-12">
                                <div class="single-col3 mar-bot-35">
                                    <div class="col2-img post-img">
                                        <a href="{{ route('recipe', ['id' => $recipe['id']]) }}"><img src="{{$recipe->imageUrl}}" alt=""></a>
                                        <div class="post-img-content">
                                            <h5>
                                                <span>{{ $recipe['name'] }}</span>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                  @else
                      You should login first to access this section !
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
