<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 27/05/17
 * Time: 01:18
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
  public $timestamps = false;

  public function user1() {
    return $this->belongsTo('app\User');
  }

  public function user2() {
    return $this->belongsTo('app\User');
  }
}