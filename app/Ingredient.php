<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
  public $timestamps = false;
  protected $primaryKey = 'id';

  protected $fillable = [
      'name', 'description', 'unit'
  ];

  public function steps() {
    return $this->belongsToMany('app\Step');//->withPivot('quantity');
  }
}