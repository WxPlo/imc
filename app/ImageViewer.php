<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageViewer extends Model
{
  protected $fillable = [
      'description'
  ];
  
  public $timestamps = false;

  public function recipe() {
    return $this->belongsTo('app\Recipe');
  }

  public function user() {
    return $this->belongsTo('App\User');
  }
}
