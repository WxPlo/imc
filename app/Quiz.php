<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 10/09/17
 * Time: 17:51
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
  public $fillable = ['question', 'image_url',
                      'answer_1', 'answer_2', 'answer_3', 'answer_4', 'answer_number',
                      'explanation'];
  public $timestamps = false;
  public $table = 'quizzes';

  public function game() {
    return $this->belongsTo('App\Game');
  }
}