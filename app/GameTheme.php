<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 09/09/17
 * Time: 00:54
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class GameTheme extends Model
{
  public $timestamps = false;

  public $fillable = ["name"];
}