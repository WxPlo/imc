<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
  protected $fillable = [
      'name', 'creation_date', 'description'
  ];
  public $timestamps = false;

  /*public function users()
  {
    return $this->belongsToMany(User::class, 'user_product')->withPivot('threshold', 'persistent');
  }*/
  public function user() {
    return $this->belongsTo('App\User');
  }

  public function steps() {
    return $this->hasMany('App\Step');
  }
  
  public function tags() {
    return $this->belongsToMany('App\Tag');
  }

  public function comments() {
    return $this->hasMany('App\Comment');
  }

  public function likes() {
    return $this->hasMany('App\Like');
  }
}
