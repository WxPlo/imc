<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 08/07/17
 * Time: 16:00
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class MiniGame extends Model
{
  public $timestamps = false;
}