<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
  public $timestamps = false;

  public function recipe() {
    return $this->belongsTo('app\Recipe');
  }

  public function user() {
    return $this->belongsTo('app\User');
  }
}
