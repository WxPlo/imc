<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 19/05/17
 * Time: 10:49
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
  protected $fillable = ['creation_date', 'text'];

  public $timestamps = false;

  public function sender() {
    return $this->belongsTo('app\User');
  }

  public function receiver() {
    return $this->belongsTo('app\User');
  }
}