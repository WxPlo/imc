<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 09/09/17
 * Time: 00:53
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class GameType extends Model
{
  public $timestamps = false;

  public $fillable = ["name"];
}