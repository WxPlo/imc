<?php

/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 18/06/17
 * Time: 16:57
 */

namespace App\Http\Controllers\Web;
use App\Recipe;
use App\Ingredient;
use App\User;
use App\Step;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Collections\RecipeCollection;
use App\Http\Collections\UserCollection;
use Storage;
use Response;
use View;
use Illuminate\Support\Facades\DB;

class RecipeController extends Controller
{
  /**
   * Display all recipes
   */
  public function index()
  {
    $recipes = Recipe::all();
    return (!$recipes->isEmpty()) ? \View::make('recipes')->with('recipes', $recipes) : \View::make('404');
  }

  /**
   * Display the list of recipes of given user
   */
  public function indexFromUserId($user_id)
  {
    $recipes = RecipeCollection::getFromUserId($user_id);
    $user = UserCollection::show($user_id);
    return \View::make('recipes')->with('recipes', $recipes)->with('user_id', $user_id)->with('user', $user);
  }

  /**
   * Display the list of favorite recipes of given user
   */
  public function indexFavoriteFromUserId($user_id)
  {
    $recipes = RecipeCollection::getFavoriteFromUserId($user_id);
    return \View::make('favorite')->with('recipes', $recipes)->with('user_id', $user_id);
  }

  public function summerRecipes()
  {
    $recipes = DB::table('recipes')
      ->join('recipe_tag', 'recipes.id', '=', 'recipe_tag.recipe_id')
      ->join('tags', 'recipe_tag.tag_id', '=', 'tags.id')
      ->join('users AS u', 'recipes.user_id', '=', 'u.id')
      ->where('tags.name', "summer")
      ->select(['recipes.id', 'recipes.name', 'u.id AS userId', 'u.name AS userName', 'u.firstname AS userFirstname', 'recipes.imageUrl'])
      ->get();

    return \View::make('summer')->with('recipes', $recipes);
  }

  public function winterRecipes()
  {
    $recipes = DB::table('recipes')
      ->join('recipe_tag', 'recipes.id', '=', 'recipe_tag.recipe_id')
      ->join('tags', 'recipe_tag.tag_id', '=', 'tags.id')
      ->join('users AS u', 'recipes.user_id', '=', 'u.id')
      ->where('tags.name', "winter")
      ->select(['recipes.id', 'recipes.name', 'u.id AS userId', 'u.name AS userName', 'u.firstname AS userFirstname', 'recipes.imageUrl'])
      ->get();

    return \View::make('winter')->with('recipes', $recipes);
  }

  public function autumnRecipes()
  {
    $recipes = DB::table('recipes')
      ->join('recipe_tag', 'recipes.id', '=', 'recipe_tag.recipe_id')
      ->join('tags', 'recipe_tag.tag_id', '=', 'tags.id')
      ->join('users AS u', 'recipes.user_id', '=', 'u.id')
      ->where('tags.name', "autumn")
      ->select(['recipes.id', 'recipes.name', 'u.id AS userId', 'u.name AS userName', 'u.firstname AS userFirstname', 'recipes.imageUrl'])
      ->get();

    return \View::make('autumn')->with('recipes', $recipes);
  }

  public function springRecipes()
  {
    $recipes = DB::table('recipes')
      ->join('recipe_tag', 'recipes.id', '=', 'recipe_tag.recipe_id')
      ->join('tags', 'recipe_tag.tag_id', '=', 'tags.id')
      ->join('users AS u', 'recipes.user_id', '=', 'u.id')
      ->where('tags.name', "spring")
      ->select(['recipes.id', 'recipes.name', 'u.id AS userId', 'u.name AS userName', 'u.firstname AS userFirstname', 'recipes.imageUrl'])
      ->get();

    return \View::make('spring')->with('recipes', $recipes);
  }

  /**
   * Get recipe from the given recipe id
   */
  public function show($recipe_id)
  {
    $recipe = RecipeCollection::get($recipe_id);
    $recommended_recipes = RecipeCollection::getRecommendedRecipes($recipe, 4);
    $ingredients = RecipeCollection::getIngredientsFromRecipeId($recipe_id);

    return ($recipe) ?
      \View::make('recipe')
        ->with('recipe', $recipe)
        ->with('recommended_recipes', $recommended_recipes)
        ->with('ingredients', $ingredients)
      : response()->view('404', [], 404);
  }

  public function createView()
  {
    $ingredients = Ingredient::all();
    return View::make('recipe-creation')->with('ingredients', $ingredients);
  }

  public function store(Request $request)
  {
    $recipe = $request;



    if (isset($recipe->name) && isset($recipe->user_id)
      && isset($recipe->creation_date) && isset($recipe->description) && isset($recipe->steps)) {
      $datetime = date("Y-m-d H:i:s", strtotime($recipe->creation_date));

      $data = new Recipe;
      $data->name = $recipe->name;
      $data->creation_date = $datetime;
      $data->description = $recipe->description;
      $user = User::find($recipe->user_id);
      $data->user()->associate($user);

      if ($recipe->image) {
        $urlImage = $this->uploadFileToS3($request);
        $data->imageUrl = $urlImage;
      } else {
        $data->imageUrl = "https://img4.hostingpics.net/pics/742253default.jpg";
      }

      $data->save();

      // ONLY WORKING WITH SINGLE TAG
      if (isset($recipe->tags)) {
        // foreach($recipe->tags as $tag) {
        //   $data->tags()->attach($tag->id, ['recipe_id' => $data->id]);
        // }
        $data->tags()->attach($recipe->tags, ['recipe_id' => $data->id]);
      }

      // Getting data in right format
      $tmp = json_decode(json_encode($recipe->steps));

      foreach ($tmp as $step) {
        $createdStep = new Step;
        $createdStep->order = $step->order;
        $createdStep->description = $step->description;
        $createdStep->difficulty = $step->difficulty;
        $createdStep->duration = $step->duration;
        $createdStep->timer_duration = $step->duration * 60;

        $createdStep->recipe()->associate($data);
        $createdStep->save();

        $tmp = json_encode($step->ingredients);
        if (isset($step->ingredients)) {
          foreach (json_decode($tmp) as $ingredient) {
            $createdStep->ingredients()->attach($ingredient->id, ['step_id' => $createdStep->id]);
          }
        }
        // echo json_encode($tmp);
      }

      $response = array('id' => $data->id);
      //echo json_encode($response);
      return \View::make('index')->with('store', "RECIPE CREATED");
    } else
      return response()->view('recipes', ['recipes' => null, 'error' => 'An error occurred when creating a recipe. Please fill all fields.'], 400);
  }

  public function uploadFileToS3(Request $request)
  {
    $this->validate($request, [
      'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ]);
    $imageName = time() . '.' . $request->image->getClientOriginalExtension();
    $image = $request->file('image');
    $s3 = Storage::disk('s3');
    $filePath = 'recipe_images/' . $imageName;
    $s3->put($filePath, file_get_contents($image), 'public');
    $imageName = Storage::disk('s3')->url($filePath);

    return $imageName;
  }

  public function updateView($id)
  {
    $difficulties = Array("10" => "Master Chef",
      "9" => "Extreme",
      "8" => "Hard",
      "7" => "Tricky",
      "6" => "Knows what you do",
      "5" => "Good",
      "4" => "Medium",
      "3" => "Reasonable",
      "2" => "Easy",
      "1" => "Poor");

    $recipe = RecipeCollection::get($id);

    if (isset($recipe) && $recipe !== null) {
      $ingredients = Ingredient::all();
      return View::make('recipe-edition')->with('recipe', $recipe)->with('ingredients', $ingredients)->with('difficulties', $difficulties);
    }
    return View::make('404');
  }

  public function update(Request $request)
  {
    if (isset($request->id) && isset($request->name) && isset($request->description) && isset($request->steps)) {
      $recipe = Recipe::find($request->id);
      if ($recipe === null) {
        http_response_code(400);
        echo("Bad Request: invalid fields"); // Status code here
      }
      $recipe->name = $request->name;
      $recipe->description = $request->description;

      /*if (isset($request['image'])) {
        $url_image = $this->uploadFileToS3($request);
        $recipe->imageUrl = $url_image;
      }
      else
        $recipe->imageUrl = "https://img4.hostingpics.net/pics/742253default.jpg";*/

      $recipe->save();


      // ONLY WORKING WITH SINGLE TAG
      /*if (isset($request['tags'])) {
        // foreach($recipe->tags as $tag) {
        //   $data->tags()->attach($tag->id, ['recipe_id' => $data->id]);
        // }
        $recipe->tags()->attach($request['tags'], ['recipe_id' => $recipe->id]);
      }*/
      // Getting data in right format

      // Steps from form
      $old_steps = json_decode(json_encode($recipe->steps));
      $new_steps = $request->steps;
      $ingredients = null;

      // Remove all deleted steps
      $a1 = array_column($old_steps, 'id');
      $a2 = array_column($new_steps, 'id');
      $steps_ids_to_delete = array_diff($a1, $a2);
      Step::whereIn('id', $steps_ids_to_delete)->delete();

      // Update and add new steps
      foreach ($new_steps AS $step) {
        $new_step = (isset($step['id'])) ? Step::find($step['id']) : new Step;
        $new_step->order = $step['order'];
        $new_step->description = $step['description'];
        $new_step->difficulty = $step['difficulty'];
        $new_step->duration = $step['duration'];

        if (!isset($step->id))
          $new_step->recipe()->associate($recipe);
        $new_step->save();

        if (isset($step->ingredients)) {
          $ingredients = json_encode($step->ingredients);
          foreach (json_decode($ingredients) as $ingredient) {
            $new_step->ingredients()->attach($ingredient->id, ['step_id' => $new_step->id]);
          }
        }
      }

      $r = RecipeCollection::get($recipe->id);

      if (isset($r) && $r !== null) {
        return \View::make('recipe')
          ->with('recipe', $r)
          ->with('store', "Recipe Updated")
          ->with('ingredients', $new_step->ingredients);
      }
      return \View::make('404');
    } else {
      http_response_code(400);
      echo("Bad Request: invalid fields"); // Status code here
    }
  }

  public function searchView() {
    $ingredients = Ingredient::all();
    $tags = Tag::all();
    foreach($ingredients as $ingredient)
      $is[$ingredient['id']] = $ingredient['name'];
    foreach($tags as $tag)
      $ts[$tag['id']] = $tag['name'];
    return \View::make('search')->with('ingredients', $is)->with('tags', $ts);
  }

  /**
   * Search from recipe name or tag
   */
  public function search(Request $request) {
    if (isset($request->text) || isset($request->ingredient) || isset($request->tag))
    $recipes = RecipeCollection::getFromNameIngredientTag($request->text, $request->ingredient, $request->tag);
      return \View::make('search')->with('recipes', $recipes);
    return \View::make('search')->with('recipes', null);
  }

  public function delete(Request $request) {
    if (isset($request->recipe_id) && isset($request->user_id)) {
      $recipe = Recipe::find($request->recipe_id);
      if ($recipe === null) // Recipe not found
        return response()->view('404', [], 404);
      $user = User::find($request->user_id);
      if ($user === null) // User not found
        return response()->view('log', ['log', 'You must be connected to delete recipes.'], 400);
      if ($recipe['user_id'] !== $user['id'] && $user['role'] !== 'admin') // Not author nor admin
        return response()->view('403', [], 403);
      else {
        $recipe->delete();
        if(Recipe::find($request->recipe_id) !== null) { // Not successfully deleted
          $ingredients = RecipeCollection::getIngredientsFromRecipeId($request->recipe_id);
          return \View::make('recipe')
              ->with('recipe', $recipe)
              ->with('ingredients', $ingredients)
              ->with('error', 'An error occurred while deleting the recipe. Please try again.');
        }
        else // Deleted
          return \View::make('recipes')
                          ->with('msg', 'The recipe has been successfully deleted.')
                          ->with('recipes', RecipeCollection::getFromUserId($request->user_id));
      }
    }
    else
      return response()->view('/recipes', ['recipes' => null], 400);
  }
}
