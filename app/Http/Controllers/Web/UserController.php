<?php

namespace App\Http\Controllers\Web;

use App\User;
use App\Http\Controllers\Controller;
use App\Http\Collections\UserCollection;
use App\Http\Collections\FollowCollection;
use Storage;
use Response;
use Cookie;
use View;
use Illuminate\Http\Request;
use App\Http\Requests;

class UserController
{
  /**
   * Get user from the given user id
   */
  public function show($user_id) {
    $user = User::where('id', $user_id)->with('recipes')->get()->first();
    $user['followings'] = sizeof(FollowCollection::getFollowings($user_id));
    $user['followers'] = sizeof(FollowCollection::getFollowers($user_id));
    return ($user !== null) ? \View::make('profile')->with('user', $user) : \View::make('404');
  }

  private function validate_login_form($email, $password) {
    if (!filter_var($email, FILTER_VALIDATE_EMAIL))
      return response()->view('log', ['log' => "Email address is in an invalid format."], 400);
    $ext = substr(strrchr($email, '.'),1);
    if (strlen($ext) < 2 || strlen($ext) > 4)
      return response()->view('log', ['log' => "Email address is in an invalid format. Length of extension must be between 2 and 4."], 400);

    $len = strlen($password);
    if ($len < '8')
      return response()->view('log', ['log' => "Your password must contain at least 8 characters!"], 400);
    else if ($len > '15')
      return response()->view('log', ['log' => "Your password must contain less than 15 characters!"], 400);
    else if (!preg_match("#[0-9]+#", $password))
      return response()->view('log', ['log' => "Your password must contain at least 1 number!"], 400);
    else if (!preg_match("#[A-Z]+#", $password))
      return response()->view('log', ['log' => "Your password must contain at least 1 capital letter!"], 400);
    else if (!preg_match("#[a-z]+#", $password))
      return response()->view('log', ['log' => "Your password must contain at least 1 lowercase letter!"], 400);
    else if (!preg_match("#[-+!*$@%_]+#", $password))
      return response()->view('log', ['log' => "Your password must contain at least 1 special character (-+!*$@%_)"], 400);
    return null;
  }

  public function login(Request $request)
  {
    // LOGIN
    if ($request->has('username') && $request->has('password'))
    {
      $validation = $this->validate_login_form($request['username'], $request['password']);
      if ($validation !== null)
        return $validation;

      $usr = UserCollection::login($request->username, $request->password);
      if (count($usr) > 0) {
        $cookie = Cookie::make('client', $usr, 3600);
        return redirect('/')->withCookie($cookie);
      }
      return response()->view('log', ['log' => "Invalid username / password"], 400);
    }


    // SIGN UP
    if ($request->has('firstname') && $request->has('name')
    && $request->has('sexe') && $request->has('age') && $request->has('country')
    && $request->has('password') && $request->has('cpassword') && $request->has('email'))
    {
      if (!ctype_alpha($request['firstname']))
        return response()->view('log', ['log' => "Firstname must only be composed of letters."], 400);
      if (!ctype_alpha($request['name']))
        return response()->view('log', ['log' => "Name must only be composed of letters."], 400);

      $filter_options = array(
        'options' => array('min_range' => 0)
      );

      if (!filter_var($request['age'], FILTER_VALIDATE_INT, $filter_options ))
        return response()->view('log', ['log' => "Age must be a positive number."], 400);

      $validation = $this->validate_login_form($request['email'], $request['password']);
      if ($validation !== null)
        return $validation;

      if (strcmp($request['password'], $request['cpassword']) !== 0)
        return response()->view('log', ['log' => "Password and confirmed password are different."], 400);
      $usr = UserCollection::signup($request);
      if (count($usr) > 0)
        return response()->view('log', ['signup' => "User registered"], 400);
      return response()->view('log', ['log' => "Invalid username/password"], 400);
    }

    // LOGOUT
    // if ($request->has('logout'))
    // {
    //   $cookie = Cookie::forget('client');
    //   return View::make('log')->withCookie($cookie);
    // }
    return response()->view('log', ['log' => "Please fill all required fields."], 400);
  }

  public function logout()
  {
      $cookie = Cookie::forget('client');
      return redirect('log')->withCookie($cookie);
  }

  public function updateProfile(Request $request)
  {
    echo $request->getContent();
    if ($request->has('firstname') && $request->has('name')
    && $request->has('sexe') && $request->has('age') && $request->has('country')
    && $request->has('email') && $request->has('id'))
    {
      $usr = UserCollection::update($request);
      if (count($usr) > 0) {
        return redirect('/profile/'.$request->id)->with('updated', "Update successful");
      }
    }
    return redirect('/profile/'.$request->id)->with('not_updated', "Failed to update");
  }
}
