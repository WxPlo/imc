<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 30/06/17
 * Time: 02:03
 */

namespace App\Http\Controllers\Web;
use App\Http\Collections\LikeCollection;
use Illuminate\Http\Request;

class LikeController
{
  public function store(Request $request)
  {
    $like = LikeCollection::store($request);
    if (isset($like))
      return back()->with('like_message', 'You\'ve liked this recipe.');
    else
      return back()->with('like_message', 'An error occurred when liking the recipe. Please try again.');
  }

  public function destroy(Request $request)
  {
    if ($request->has('recipe_id') && $request->has('user_id')) {

      if (LikeCollection::destroy($request['recipe_id'], $request['user_id']))
        return back()->with('like_message', 'You\'ve unliked this recipe.');
      else
        return back()->with('like_message', 'An error occurred when unliking the recipe. Please try again.');
    }
  }
}