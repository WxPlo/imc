<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 19/06/17
 * Time: 01:35
 */

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use App\Http\Collections\CommentCollection;
use Illuminate\Http\Request;
use App\User;
use App\Recipe;
use App\Comment;
use Response;

class CommentController extends Controller
{
  /**
   * Store a new comment
   */
  public function store(Request $request)
  {
    $comment = CommentCollection::store($request);
    if ($comment !== null && get_object_vars($comment))
      return redirect()->back()->with('comment_message', 'Your comment has been successfully post.');
    else
      return response()->view('recipes',
                              ['recipes' => null,
                               'error' => 'An error occurred when posting the comment. Please try again.'], 400);
  }
}
