<?php

namespace App\Http\Controllers\Web;

use App\Http\Collections\FollowCollection;
use View;
use App\User;
use Illuminate\Http\Request;

class FollowController
{
  public function showFollowings($id) {
    $user = User::find($id);
    $user['followings'] = json_decode(FollowCollection::getFollowings($id), true);
    return ($user !== null) ? \View::make('profile-followings')->with('user', $user) : \View::make('404');
  }

  public function showFollowers($id) {
    $user = User::find($id);
    $user['followers'] = json_decode(FollowCollection::getFollowers($id), true);
    return ($user !== null) ? \View::make('profile-followers')->with('user', $user) : \View::make('404');
  }
}
