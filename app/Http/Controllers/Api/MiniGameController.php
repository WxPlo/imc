<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 08/07/17
 * Time: 15:58
 */

namespace App\Http\Controllers\Api;


use App\MiniGame;
use Response;

class MiniGameController
{
  public function index() {
    $mini_games = MiniGame::all();
    return Response::json($mini_games);
  }
}