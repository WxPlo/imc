<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 09/09/17
 * Time: 01:26
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Collections\GameCollection;


class GameController extends Controller
{
  /**
   * Get all games
   */
  public function index() {
    return GameCollection::getGames();
  }

  /**
   * Get games from given theme id
   */
  public function indexFromThemeId($theme_id) {
    return GameCollection::getGamesFromThemeId($theme_id);
  }
}