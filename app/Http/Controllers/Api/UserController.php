<?php

namespace App\Http\Controllers\Api;
use App\Http\Collections\UserCollection;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Response;

class UserController extends Controller
{
  public function index()
  {
    $users = User::from('users')
      ->select(['id', 'firstname', 'name', 'sexe', 'age', 'country', 'email', 'created_at', 'updated_at'])->get();
    return Response::json($users);
  }

  public function login(Request $request)
  {
    if ($request->has('username') && $request->has('password'))
    {
      //$hashed_password = md5($request->password);
      //$req = User::where('email', $request->username)->where('password', $hashed_password)->first();
      $req = UserCollection::login($request->username ,$request->password);
      if (count($req) > 0) {
        $req->api_token = str_random(60);
        $req->save();
        return Response::json($req);
      }
      else {
        return response("User unknown")->setStatusCode(401, 'Invalid username/password');
      }
    }
    else {
      return response("Invalid fields")->setStatusCode(401, 'Invalid username/password');
    }



    // For changing password
    if ($request->has('newPassword') && $request->has('username'))
    {
      $req = User::where('email', $request->username)->first();
      $hashed_password = md5($request->newPassword);
      $req->password = $hashed_password;
      $req->save();
      echo "Success";
    }

    if ($request->has('logState') && $request->has('email'))
    {
      $req = User::where('email', $request->username)->first();
      $req->password = "";
      echo "Disconnected";
    }

  }

  public function signUp(Request $request)
  {
    if ($request->has('firstname') && $request->has('name')
    && $request->has('sexe') && $request->has('age') && $request->has('country')
    && $request->has('password') && $request->has('email'))
    {
      // $hashed_password = md5($request->password);
      //
      // $data = new User;
      // $data->firstname = $request->firstname;
      // $data->name = $request->name;
      // $data->sexe = $request->sexe;
      // $data->age = $request->age;
      // $data->country = $request->country;
      // $data->password = $hashed_password;
      // $data->email = $request->email;
      // $data->api_token = str_random(60);
      //
      // $data->save();
      // $response = array('id' => $data->id);
      // return Response::json($response);
      $ret = UserCollection::signup($request);
      return (count($ret) > 0 ? Response::json($ret) : response("Invalid fields")->setStatusCode(401, 'Invalid fields'));
    }
    else {
      return response("Invalid fields")->setStatusCode(401, 'Invalid fields');
    }
  }

  public function show($id) {
    $user = User::where('id', $id)->with('recipes')->get()->first();
    return Response::json($user);
  }
}

?>
