<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\User;
use App\Recipe;
use App\ImageViewer;
use Illuminate\Http\Request;
use Storage;
use Response;
use Illuminate\Support\Facades\DB;

class ImageViewerController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$imageView = json_decode($request->input("data"));
		if (isset($imageView->recipe_id) && isset($imageView->user_id)) {
          $imageviewer = new ImageViewer;
		  if (isset($imageView->description)) {
			 $imageviewer->description =  $imageView->description;
		  }
		  $urlImage = $this->uploadFileToS3($request);
          $imageviewer->imageUrl = $urlImage;
		  
          $recipe = Recipe::find($imageView->recipe_id);
          $user = User::find($imageView->user_id);
          $imageviewer->recipe()->associate($recipe);
          $imageviewer->user()->associate($user);
          $imageviewer->save();
		  
		  return Response::json($imageviewer);
          //echo (Response(json_encode($imageviewer), 200));
        }
		echo (Response(json_encode($imageView), 400));
    }
	
	public function getByRecipe($recipeId) {
		$images = ImageViewer::where('recipe_id', $recipeId)->with('user')->get();
		
		echo(json_encode($images));
	}
	
	public function uploadFileToS3(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $image = $request->file('image');
        $s3 = Storage::disk('s3');
        $filePath = 'recipe_images/' . $imageName;
        $s3->put($filePath, file_get_contents($image), 'public');
        $imageName = Storage::disk('s3')->url($filePath);

        return $imageName;
    }
	
	public function destroy($id)
	{
		ImageViewer::where('id', $id)->delete();

		$image = ImageViewer::where('id', $id)->first();
		if (!isset($image))
		  http_response_code(200);
		else
		  echo (Response("Error when deleting.", 400));
	}
}