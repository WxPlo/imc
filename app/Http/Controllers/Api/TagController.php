<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Tag;


class TagController extends Controller
{
  public function getTags() {
      $tags = Tag::all();
      echo (json_encode($tags));
  }
}
?>
