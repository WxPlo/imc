<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Ingredient;


class IngredientController extends Controller
{
  public function getIngredients() {
      $ingredients = Ingredient::orderBy('name', 'ASC')->get();
      echo (json_encode($ingredients));
  }
}


?>
