<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 19/05/17
 * Time: 10:59
 */

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Message;
use App\User;
use App\Conversation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
  public function getMessage($messageId) {
    $message = Message::find($messageId);
    if (isset($message)) {
      $message['sender'] = User::find($message['sender_id']);
      $message['receiver'] = User::find($message['receiver_id']);

      echo (json_encode($message));
    }
    else {
      http_response_code(400);
      echo "ERROR: Message " . $messageId . " not found.";
    }
  }

  /**
   * Get sent and received messages of given user
   */
  public function getUserMessages($userId) {
    $messages = Message::where('sender_id', $userId)
      ->orWhere('receiver_id', $userId)
      ->orderBy('creation_date', 'desc')
      ->get();
    foreach ($messages as $message) {
      $message['sender'] = User::find($message['sender_id']);
      $message['receiver'] = User::find($message['receiver_id']);
    }

    echo (json_encode($messages));
  }

  /**
   * Get sent messages of given user
   */
  public function getUserSentMessages($userId) {
    $user = User::find($userId);
    $messages = Message::where('sender_id', $userId)
      ->orderBy('creation_date', 'desc')
      ->get();
    foreach ($messages as $message) {
      $message['sender'] = $user;
      $message['receiver'] = User::find($message['receiver_id']);
    }

    echo (json_encode($messages));
  }

  /**
   * Get received messages of given user
   */
  public function getUserReceivedMessages($userId) {
    $user = User::find($userId);
    $messages = Message::where('receiver_id', $userId)->get();
    foreach ($messages as $message) {
      $message['sender'] = User::find($message['sender_id']);
      $message['receiver'] = $user;
    }

    echo (json_encode($messages));
  }

  public function getUsersMessages($userId, $user2Id) {
    $messages = Message::where([['sender_id', $userId], ['receiver_id', $user2Id]])
                        ->orWhere([['sender_id', $user2Id], ['receiver_id', $userId]])
                        ->orderBy('creation_date', 'desc')
                        ->get();

    foreach ($messages as $message) {
      $message['sender'] = User::find($message['sender_id']);
      $message['receiver'] = User::find($message['receiver_id']);
    }

    echo (json_encode($messages));
  }

  /**
   * Get conversations of given user (the last message of each contact)
   *
   * SELECT *
   * FROM messages
   * WHERE id in (
   *    select max(id) from messages where sender_id = 43 GROUP BY receiver_id
   *    union all
   *    select max(id) from messages where receiver_id = 43 GROUP BY sender_id
   * );
   *
   * @param $userId
   */
  /*public function getConversations($userId) {

    $receivedIds = Message::select(DB::raw('max(id)'))
      ->where('sender_id', $userId)
      ->groupBy('receiver_id');

    $sentIds = Message::select(DB::raw('max(id)'))
      ->where('receiver_id', $userId)
      ->groupBy('sender_id');

    $ids = $receivedIds->union($sentIds)->get();

    $messages = Message::whereIn('id', $ids)
      ->orderBy('creation_date', 'desc')
      ->get();

    foreach ($messages as $message) {
      $message['sender'] = User::find($message['sender_id']);
      $message['receiver'] = User::find($message['receiver_id']);
    }

    echo (json_encode($messages));
  }*/


  /**
   *
   *
   * SELECT *
   * FROM messages where id in (
   *    SELECT max(id)
   *    FROM messages
   *    WHERE sender_id = 43 OR receiver_id = 43
   *    GROUP BY conversation_id
   * );
   *
   * @param $userId
   */
  public function getConversations($userId) {

    $ids = Message::select(DB::raw('max(id)'))
      ->where('sender_id', $userId)
      ->orWhere('receiver_id', $userId)
      ->groupBy('conversation_id')
      ->get();

    $messages = Message::whereIn('id', $ids)
      ->orderBy('creation_date', 'desc')
      ->get();

    foreach ($messages as $message) {
      $message['sender'] = User::find($message['sender_id']);
      $message['receiver'] = User::find($message['receiver_id']);
    }

    echo (json_encode($messages));
  }

  public function store(Request $request)
  {

    if ($request->has('sender_id') && $request->has('text') && $request->has('creation_date')) {

      $receiver = ($request->has('receiver_id')) ?
        User::find($request->receiver_id)
        : User::where('email', $request->receiver_email)->get()->first();

      $receiver_id = ($request->has('receiver_id')) ?
        $request->receiver_id
        : $receiver->id;

      $datetime = date("Y-m-d H:i:s", strtotime($request->creation_date));

      $message = new Message;
      $sender = User::find($request->sender_id);

      $message->creation_date = $datetime;
      $message->text = $request->text;
      $message->sender()->associate($sender);
      $message->receiver()->associate($receiver);
      $conversation = Conversation::where([['user1_id', $request->sender_id], ['user2_id', $receiver_id]])
        ->orWhere([['user2_id', $request->sender_id], ['user1_id', $receiver_id]])
        ->get()->first();
      if ($conversation) {
        $message->conversation_id = $conversation->id;
      } else {
        $conv = new Conversation;
        $conv->user1()->associate($sender);
        $conv->user2()->associate($receiver);
        $conv->save();
        $message->conversation_id = $conv->id;
      }

      $message->save();
      echo json_encode($message);
      /*echo json_encode($request->text);*/
      //}
      //
    } else {
      http_response_code(400);
      echo("Bad Request: Invalid field.");
    }
  }

  public function destroy($messageId) {
    $message = Message::find($messageId);
    if (isset($message)) {
      $message->delete();
      $msg = Message::find($messageId);
      if (!isset($msg))
        http_response_code(200);
      else
        echo (Response("Error when deleting.", 400));
    }
    else
      echo (Response("Error: " . $messageId . " not found.", 400));

  }
}