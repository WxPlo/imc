<?php

namespace App\Http\Controllers\Api;
use App\Http\Collections\RecipeCollection;
use App\Http\Controllers\Controller;
use App\Recipe;
use App\User;
use App\Step;
use App\Comment;
use Illuminate\Http\Request;
use Storage;
use Response;
use Illuminate\Support\Facades\DB;

class RecipeController extends Controller
{

  public function summerRecipes()
  {
    $recipes = DB::table('recipes')
    ->join('recipe_tag', 'recipes.id', '=', 'recipe_tag.recipe_id')
    ->join('tags', 'recipe_tag.tag_id', '=', 'tags.id')
    ->join('users AS u', 'recipes.user_id', '=', 'u.id')
    ->where('tags.name', "winter")
    ->select(['recipes.id', 'recipes.name', 'u.id AS user.id', 'u.name AS user.name', 'u.firstname AS user.firstname'])
    ->get();

    return $recipes;
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resuponse
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $recipe = json_decode($request->input("recipe"));
		//echo $request->input("recipe");
        if (isset($recipe->name) && isset($recipe->user_id)
        && isset($recipe->creation_date) && isset($recipe->description) && isset($recipe->steps))
        {
          $datetime =  date("Y-m-d H:i:s", strtotime($recipe->creation_date));

          $data = new Recipe;
          $data->name = $recipe->name;
          $data->creation_date = $datetime;
          $data->description = $recipe->description;
          $user = User::find($recipe->user_id);
          $data->user()->associate($user);

          if ($request->image){
            $urlImage = $this->uploadFileToS3($request);
            $data->imageUrl = $urlImage;
          }
          else
              $data->imageUrl = "https://img4.hostingpics.net/pics/742253default.jpg";

          $data->save();
    		  if (isset($recipe->tags)) {

    			  foreach($recipe->tags as $tag) {
    				$data->tags()->attach($tag->id, ['recipe_id' => $data->id]);
    			  }
    		  }

          foreach($recipe->steps as $step) {
              $createdStep = new Step;
              $createdStep->order = $step->order;
              $createdStep->description = $step->description;
              $createdStep->difficulty = $step->difficulty;
              $createdStep->duration = $step->duration;
              $createdStep->timer_duration = $step->timer_duration->hour * 3600 + $step->timer_duration->minute * 60
                                            + $step->timer_duration->second;
              $createdStep->mini_game_id = $step->mini_game_id;
              $createdStep->recipe()->associate($data);
              $createdStep->save();
              if (isset($step->ingredients)) {
                foreach($step->ingredients as $ingredient) {
                    $createdStep->ingredients()->attach($ingredient->id, ['step_id' => $createdStep->id]);
                }
              }
          }

          $response = array('id' => $data->id);
          echo json_encode($response);
        }
        else {
          http_response_code(400);
          echo ("Bad Request: invalid fields");; // Status code here
        }
    }

  /**
   * Get recipe from the given recipe id
   */
  public function show($recipe_id)
  {
    return Response::json(RecipeCollection::get($recipe_id));
  }

    /**
     * Get recipes from the given user id
     */
    public function showFromUserId($userId)
    {
      $recipes = Recipe::where('user_id', $userId)->with('steps.ingredients', 'steps.mini_game', 'user', 'tags')->get();

      foreach($recipes as $recipe) {
        $steps = $recipe['steps'];
        foreach($steps as $step) {
          if (isset($step['timer_duration'])) {
            $timer_duration = $step['timer_duration'];
            $array = Array('hour' => floor($timer_duration / 3600), 'minute' => floor($timer_duration / 60), 'second' => $timer_duration % 60);
            $step['timer_duration'] = $array;
          }
        }
      }
      return Response::json($recipes);

      /*$user = User::find($userId);
      $recipes = Recipe::where('user_id', $userId)->get();
      foreach($recipes as $value) {
          $value['owner'] = $user;
          $steps = Step::where('recipe_id', $value['id'])->get();
          foreach($steps as $step) {
              if (isset($step['timer_duration'])) {
                  $timer_duration = $step['timer_duration'];
                  $array = Array('hour' => floor($timer_duration / 3600), 'minute' => floor($timer_duration / 60), 'second' => $timer_duration % 60);
                  $step['timer_duration'] = $array;
              }
              $ingredients = $step->ingredients()->get();
              $step['ingredients'] = $ingredients;
          }

          $value['steps'] = $steps;
        $value['tags'] = $value->tags()->get();
      }
      echo (json_encode($recipes));*/
    }

  /**
   * Get all recipes
   */
    public function showAll()
    {
      $recipes = Recipe::with('steps.ingredients', 'steps.mini_game', 'user', 'tags', 'comments', 'comments.user')->get();

      foreach($recipes as $recipe) {
        $steps = $recipe['steps'];
        foreach($steps as $step) {
          if (isset($step['timer_duration'])) {
            $timer_duration = $step['timer_duration'];
            $array = Array('hour' => floor($timer_duration / 3600), 'minute' => floor($timer_duration / 60), 'second' => $timer_duration % 60);
            $step['timer_duration'] = $array;
          }
        }
      }
      return Response::json($recipes);

      /*
       $recipes = Recipe::all();
       foreach($recipes as $value) {
       $value['owner'] = $value->user()->get()->first();
       $steps = Step::where('recipe_id', $value['id'])->get();
       foreach($steps as $step) {
         if (isset($step['timer_duration'])) {
           $timer_duration = $step['timer_duration'];
           $array = Array('hour' => floor($timer_duration / 3600), 'minute' => floor($timer_duration / 60), 'second' => $timer_duration % 60);
           $step['timer_duration'] = $array;
         }
         $ingredients = $step->ingredients()->get();
         $step['ingredients'] = $ingredients;
       }
       $value['steps'] = $steps;
       $value['tags'] = $value->tags()->get();
      }
      echo (json_encode($recipes));*/
    }

  /**
   * Get recipes with the given tag
   */
  public function showFromTagName($tagName)
  {
    $recipes =
      Recipe::from('recipes AS r')
        ->join('recipe_tag AS rt', 'rt.recipe_id', '=', 'r.id')
        ->join('users AS u', 'r.user_id', '=', 'u.id')
        ->join('tags AS t', 'rt.tag_id', '=', 't.id')
        ->select(['r.id', 'r.name', 'r.description', 'r.creation_date', 'r.imageUrl', 'u.id AS user_id', 'u.name AS user_name', 'u.firstname AS user_firstname'])
        ->where('t.name', $tagName)->get();

    foreach($recipes as $value) {
      $value['user'] = $value->user()->get()->first();
      $steps = Step::where('recipe_id', $value['id'])->get();
      foreach($steps as $step) {
        if (isset($step['timer_duration'])) {
          $timer_duration = $step['timer_duration'];
          $array = Array('hour' => floor($timer_duration / 3600), 'minute' => floor($timer_duration / 60), 'second' => $timer_duration % 60);
          $step['timer_duration'] = $array;
        }
        $ingredients = $step->ingredients()->get();
        $step['ingredients'] = $ingredients;
      }
      $value['steps'] = $steps;
      $value['tags'] = $value->tags()->get();
    }
    return Response::json($recipes);
  }

  /**
   *
   */
  public function showAllFromTagNameNew($tagName)
  {
    $recipes =
        Recipe::from('recipes AS r')
          ->join('recipe_tag AS rt', 'rt.recipe_id', '=', 'r.id')
          ->join('users AS u', 'r.user_id', '=', 'u.id')
          ->join('tags AS t', 'rt.tag_id', '=', 't.id')
          ->select(['r.id', 'r.name', 'r.description', 'u.id AS user_id', 'u.name AS user_name', 'u.firstname AS user_firstname'])
          ->where('t.name', $tagName)->get();

    return Response::json($recipes);
  }

  public function indexFavoriteFromUserId($user_id)
  {
    $recipes = Recipe::from('recipes AS r')
      ->join('likes AS l', 'l.recipe_id', '=', 'r.id')
      ->join('users AS u', 'r.user_id', '=', 'u.id')
      ->where('l.user_id', $user_id)
      ->select(['r.id', 'r.name', 'r.imageUrl', 'u.id AS user.id', 'u.name AS user.name', 'u.firstname AS user.firstname'])
      ->get();
    return Response::json($recipes);
  }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {

      if ($request->has('id') && $request->has('name')
      && $request->has('description') && $request->has('steps'))
      {
        $recipe = Recipe::find($id);
        $recipe->name = $request->name;
        $recipe->description = $request->description;
        foreach($request->steps as $step) {
          if (isset($step['id'])) {
              $stepToChange = $recipe->steps()->where('id', $step['id'])->first();
              $stepToChange->order = $step['order'];
              $stepToChange->description = $step['description'];
              $stepToChange->duration = $step['duration'];
              $stepToChange->difficulty = $step['difficulty'];
              $stepToChange->timer_duration = $step['timer_duration']['hour'] * 3600 + $step['timer_duration']['minute'] * 60
                                          + $step['timer_duration']['second'];
              $stepToChange->save();

              $ingredients = $stepToChange->ingredients()->detach();

              foreach($step['ingredients'] as $ingredient) {
                  $stepToChange->ingredients()->attach($ingredient['id'], ['step_id' => $stepToChange->id]);
              }
              $stepToChange->save();
          }
        }
        $tags = $recipe->tags()->detach();
        foreach($request->tags as $tag) {
          $recipe->tags()->attach($tag['id'], ['recipe_id' => $recipe->id]);
        }
        $recipe->save();
        echo (json_encode(Array('id' => $recipe->id)));
      }
      else {
        http_response_code(400);
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function uploadFileToS3(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $image = $request->file('image');
        $s3 = Storage::disk('s3');
        $filePath = 'recipe_images/' . $imageName;
        $s3->put($filePath, file_get_contents($image), 'public');
        $imageName = Storage::disk('s3')->url($filePath);

        return $imageName;
    }

    public function indexIngredientsFromRecipeId($id)
    {
      $ingredients = RecipeCollection::getIngredientsFromRecipeId($id);
      return Response::json($ingredients);
    }
}
