<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 14/05/17
 * Time: 00:10
 */

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Like;
use App\Http\Collections\LikeCollection;
use Illuminate\Http\Request;
use Response;

class LikeController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $like = LikeCollection::store($request);
    if (isset($like))
      return Response::json($like);
    else
      return Response::json([$like], 400);
  }

  public function getLike($recipeId, $userId)
  {
    $like = Like::where([['recipe_id', $recipeId], ['user_id', $userId]])->first();

    if (isset($like)) {
      http_response_code(200);
      echo json_encode($like);
    } else {
      http_response_code(400);
    }
  }

  public function getLikes($recipeId) {
    return Response::json(LikeCollection::getLikes($recipeId));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($recipeId, $userId)
  {
    $is_destroyed = LikeCollection::destroy($recipeId, $userId);
    return ($is_destroyed) ? Response("Error when deleting.", 400) : http_response_code(200);
  }
}
