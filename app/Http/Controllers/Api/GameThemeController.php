<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 09/09/17
 * Time: 12:26
 */

namespace App\Http\Controllers\Api;


use App\GameTheme;
use App\Http\Controllers\Controller;
use Response;

class GameThemeController extends Controller
{
  public function index() {
    $game_themes = GameTheme::from('game_themes')
      ->orderBy('name', 'ASC')
      ->get();

    return Response::json($game_themes);
  }
}