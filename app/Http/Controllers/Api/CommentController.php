<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Http\Collections\CommentCollection;
use Illuminate\Http\Request;
use App\User;
use App\Recipe;
use App\Comment;
use Response;

class CommentController extends Controller
{

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $comment = null;
      if ($request->has('recipe_id') && $request->has('user_id') && $request->has('data'))
        $comment = CommentCollection::store($request);
      return Response::json($comment);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showAll($recipe_id)
    {
      return Response::json(CommentCollection::index($recipe_id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($commentId)
    {
        Comment::find($commentId)->delete();

        $comment = Comment::find($commentId)->first();
        if (!isset($comment))
          http_response_code(200);
        else
          echo (Response("Error when deleting.", 400));

    }
}
