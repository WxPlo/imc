<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Response;
use App\Http\Requests;
use Storage;
use Illuminate\Support\Facades\DB;


class ProfileController extends Controller
{
  public function editProfile($id, Request $request) {

    $data = User::where('id', $id)->first();
    if ($request->has('firstname'))
      $data->firstname = $request->firstname;
    if ($request->has('name'))
      $data->name = $request->name;
    if ($request->has('age'))
      $data->age = $request->age;
    if ($request->has('country'))
      $data->country = $request->country;
    if ($request->has('newPass') && $request->has('oldPass') && md5($request->oldPass)->first())
      $data->password = md5($request->newPass);
    if ($request->has('sexe'))
      $data->sexe = $request->sexe;
    $data->save();
    $response = array('id' => $data->id);
    return Response::json($response);
  }
  
  public function updateProfilePicture(Request $request) {
	  
		$profil = json_decode($request->input("profile"));
        if (isset($profil->id))
        {
			$data = User::where('id', $profil->id)->first();
			$urlImage = $this->uploadFileToS3($request);
			$data->pictureUrl = $urlImage;
			$data->save();
			$response = array('id' => $data->id);
			return Response::json($response);
		}
		else {
			return Response::json("Error", 400);
		}
  }
  
  public function uploadFileToS3(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $image = $request->file('image');
        $s3 = Storage::disk('s3');
        $filePath = 'recipe_images/' . $imageName;
        $s3->put($filePath, file_get_contents($image), 'public');
        $imageName = Storage::disk('s3')->url($filePath);

        return $imageName;
    }
}
?>
