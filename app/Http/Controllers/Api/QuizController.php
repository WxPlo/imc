<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 10/09/17
 * Time: 18:10
 */

namespace App\Http\Controllers\Api;


use App\Http\Collections\QuizCollection;
use App\Http\Controllers\Controller;
use Response;
use Illuminate\Http\Request;


class QuizController extends Controller
{
  /**
   * Get all quiz data
   */
  public function index() {
    $quizzes = QuizCollection::getQuizzes();
    return Response::json($quizzes);
  }

  /**
   * Get quiz data from a given game_id and number of questions randomly chosen
   */
  public function indexFromGameIdNumber($game_id, $number) {
    $quizzes = QuizCollection::getQuizFromGameIdNumber($game_id, $number);
    return Response::json($quizzes);
  }

  /**
   * Get best score of a given user and game
   */
  public function getRecord($game_id, $user_id) {
    $record = QuizCollection::getRecord($game_id, $user_id);
    return Response::json($record);
  }

  public function updateRecord(Request $request) {
    $r = QuizCollection::updateRecord($request);
    return Response::json($r);
  }
}