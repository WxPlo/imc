<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Http\Collections\FollowCollection;
use Illuminate\Http\Request;
use App\User;
use App\Follow;
use Response;

class FollowController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if ($request->has('follower_id') && $request->has('user_id')) {
          $follow = new Follow;
          $follower = User::find($request->follower_id);
          $user = User::find($request->user_id);
          $follow->follower()->associate($follower);
          $follow->user()->associate($user);
          $follow->save();
          echo (Response(json_encode($follow), 200));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showFollowers($userId)
    {
        $followers = Follow::where('follower_id', $userId)->get();
		    $array = [];
        foreach ($followers as $follower) {
          array_push($array, User::find($follower['user_id']));
        }
        return Response::json($array);
        //echo (json_encode($array));

        //return Response::json(FollowCollection::getFollowers($userId));
    }

	public function showFollowings($userId) {
		$followings = Follow::where('user_id', $userId)->get();
		$array = [];
    foreach ($followings as $following) {
      array_push($array, User::find($following['follower_id']));
    }
    return Response::json($array);
        //echo (json_encode($array));
    //return Response::json(FollowCollection::getFollowings($userId));
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($userId, $followerId)
    {
      Follow::where([['user_id', $userId], ['follower_id', $followerId]])->delete();

      $follow = Follow::where([['user_id', $userId], ['follower_id', $followerId]])->first();
      if (!isset($follow))
        http_response_code(200);
      else
        echo (Response("Error when deleting.", 400));
    }
}
