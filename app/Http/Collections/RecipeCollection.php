<?php

/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 19/06/17
 * Time: 00:23
 */

namespace App\Http\Collections;
use App\Recipe;

class RecipeCollection
{
  /**
   * Display the list of favorite recipes of a given user
   */
  public static function getFavoriteFromUserId($user_id)
  {
    return Recipe::from('recipes AS r')
      ->join('likes AS l', 'l.recipe_id', '=', 'r.id')
      ->join('users AS u', 'r.user_id', '=', 'u.id')
      ->where('l.user_id', $user_id)
      ->select(['r.id', 'r.name', 'u.id AS user_id', 'u.name AS user.name', 'u.firstname AS user.firstname', 'r.imageUrl'])
      ->get();
  }

  /**
   * Display the list of recipes of a given user
   */
  public static function getFromUserId($user_id)
  {
    return Recipe::from('recipes AS r')
      ->join('users AS u', 'r.user_id', '=', 'u.id')
      ->where('r.user_id', $user_id)
      ->select(['r.id', 'r.name', 'u.id AS user.id', 'u.name AS user.name', 'u.firstname AS user.firstname', 'r.imageUrl'])
      ->get();
  }

  /**
   * Get recipes whose name, ingredient and tag match.
   */
  public static function getFromNameIngredientTag($name, $ingredient_id, $tag_id)
  {
    $query =
      Recipe::from('recipes AS r')
        ->join('recipe_tag AS rt', 'rt.recipe_id', '=', 'r.id')
        ->join('steps AS s', 's.recipe_id', '=', 'r.id')
        ->join('ingredient_step AS is', 'is.step_id', '=', 's.id')
        ->join('ingredients AS i', 'is.ingredient_id', '=', 'i.id')
        ->join('users AS u', 'r.user_id', '=', 'u.id')
        ->join('tags AS t', 'rt.tag_id', '=', 't.id');

    if (isset($name))
      $query = $query->where('r.name', 'LIKE', '%'.$name.'%');
    if (isset($ingredient_id))
      $query = $query->where('i.id', '=', $ingredient_id);
    if (isset($name))
      $query = $query->where('t.id', '=', $tag_id);

    return $query
            ->select(['r.id', 'r.name', 'r.imageUrl', 'u.id AS user_id', 'u.name AS user_name', 'u.firstname AS user_firstname'])
            ->get();
  }

  /**
   * Get recipe from the given recipe id.
   * Add difficulty, duration and comments.
   *
   * @param $recipe_id
   */
  public static function get($recipe_id)
  {
    $recipe = Recipe::where('id', $recipe_id)
      ->with('steps.ingredients', 'steps.mini_game', 'user', 'tags', 'comments', 'comments.user', 'likes')
      ->get()
      ->first();

    if (isset($recipe['steps'])) {

      $steps = $recipe['steps'];
      $duration = Array('hour' => 0, 'minute' => 0, 'second' => 0);
      $sum_difficulty = 0;

      foreach($steps as $step) {
        // Set duration (hours, minutes, seconds) of each step.
        if (isset($step['timer_duration'])) {
          $timer_duration = $step['timer_duration'];
          $array = Array('hour' => floor($timer_duration / 3600), 'minute' => floor($timer_duration / 60), 'second' => $timer_duration % 60);
          $step['timer_duration'] = $array;
          // Add to total_duration
          $duration['hour'] += $array['hour'];
          $duration['minute'] += $array['minute'];
          $duration['second'] += $array['second'];
          $sum_difficulty += $step['difficulty'];
        }
      }
      if (count($steps) != 0)
        $recipe['difficulty'] = round($sum_difficulty / count($steps));
      $recipe['duration'] = $duration;
    }

    return $recipe;
  }

  /**
   *
   * Get recommended recipes from given recipe
   *
   * @param $recipe: Recipe. Must contain tags and user_id.
   * @param $taken_nb: Number of recommended recipes to return
   */
  public static function getRecommendedRecipes($recipe, $taken_nb) {
    $recommended_recipes = Recipe::where('user_id', $recipe['user_id'])
      ->take($taken_nb)
      ->with('user')
      ->get();

    if ($recommended_recipes->isEmpty() || count($recommended_recipes) < $taken_nb) {
      // Get recipes with same tags
      if (isset($recipe['tags']) && count($recipe['tags']) > 0) {
        foreach ($recipe['tags'] AS $tag) {
          if (count($recommended_recipes) < 4) {
            $tag_recipes = Recipe::from('recipes AS r')
              ->join('recipe_tag AS rt', 'rt.recipe_id', '=', 'r.id')
              ->join('tags AS t', 'rt.tag_id', '=', 't.id')
              ->join('users AS u', 'r.user_id', '=', 'u.id')
              ->where('t.id', $tag['id'])
              ->select(['r.id', 'r.name', 'u.id AS user.id', 'u.name AS user.name', 'u.firstname AS user.firstname'])
              ->limit($taken_nb)
              ->get();
            $recommended_recipes = $recommended_recipes->merge($tag_recipes);
          }
        }
      }
      else {
        // Get random recipes
        $recommended_recipes = $recommended_recipes->merge(Recipe::all()->take($taken_nb));
      }
    }
    return $recommended_recipes;
  }

  public static function getIngredientsFromRecipeId($id) {
    return Recipe::from('recipes AS r')
      ->join('steps AS s', 's.recipe_id', '=', 'r.id')
      ->join('ingredient_step AS is', 'is.step_id', '=', 's.id')
      ->join('ingredients AS i', 'is.ingredient_id', '=', 'i.id')
      ->where('r.id', $id)
      ->select(['is.quantity', 'i.name', 'i.unit', 'i.image_url', 'i.id', 'i.description']) // TODO: Distinct or not?
      ->distinct()
      ->get();
  }
}
