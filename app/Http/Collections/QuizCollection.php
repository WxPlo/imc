<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 10/09/17
 * Time: 18:03
 */

namespace App\Http\Collections;


use App\QuizRecord;
use App\Quiz;
use Illuminate\Support\Facades\DB;

class QuizCollection
{
  public static function getQuizzes() {
    return Quiz::all();
  }

  /**
   *
   * Get quiz
   *
   * @param $game_id
   * @param $number Number of questions randomly chosen
   */
  public static function getQuizFromGameIdNumber($game_id, $number) {
    $quizzes = Quiz::from('quizzes')
      ->where('game_id', $game_id)
      ->orderBy(DB::raw('RAND()'))
      ->take($number)
      ->get();

    return $quizzes;
  }

  public static function getRecord($game_id, $user_id) {
    $record = QuizRecord::from('quiz_records')
      ->where([['game_id', $game_id], ['user_id', $user_id]])
      ->first();

    return $record;
  }

  public static function updateRecord($record) {

    if ($record->has('id')) {
      $r = QuizRecord::find($record->id);
      $r->best_score = $record->best_score;
      $r->best_time = $record->best_time;
      $r->best_row = $record->best_row;
      return $r->save();
    }
  }
}