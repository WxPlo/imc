<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 19/06/17
 * Time: 01:30
 */

namespace App\Http\Collections;
use Illuminate\Http\Request;
use App\User;
use App\Recipe;
use App\Comment;

class CommentCollection
{
  /**
   * Store a new comment
   */
  public static function store(Request $request)
  {
    $comment = null;
    if ($request->has('recipe_id') && $request->has('user_id') && $request->has('data')) {
      $comment = new Comment;

      $recipe = Recipe::find($request->recipe_id);
      if ($recipe === null) // Recipe not found
        return null;
      $user = User::find($request->user_id);
      if ($user === null) // User not found
        return null;

      $comment->user()->associate($user);
      $comment->recipe()->associate($recipe);
      $comment->data = $request->data;
      $comment->creation_date = date("Y-m-d H:i:s");
      $comment->save();
    }
    return $comment;
  }

  public static function index($recipe_id)
  {
    return Comment::where('recipe_id', $recipe_id)->with('user')->get();
  }

}
