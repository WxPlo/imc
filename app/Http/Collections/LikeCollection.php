<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 30/06/17
 * Time: 01:25
 */

namespace App\Http\Collections;
use App\Like;
use App\Recipe;
use App\User;
use Illuminate\Http\Request;

class LikeCollection
{
  public static function store(Request $request)
  {
    if ($request->has('recipe_id') && $request->has('user_id')) {
      $like = new Like;
      $recipe = Recipe::find($request->recipe_id);
      $user = User::find($request->user_id);
      $like->recipe()->associate($recipe);
      $like->user()->associate($user);
      $like->save();
      return $like;
    }
    return null;
  }

  public static function getLikes($recipeId) {
    return Like::where('recipe_id', $recipeId)->with('user')->get();
  }

  public static function destroy($recipe_id, $user_id)
  {
    return Like::where([['recipe_id', $recipe_id], ['user_id', $user_id]])->delete();
  }
}