<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 08/09/17
 * Time: 19:29
 */

namespace App\Http\Collections;

use App\Game;

class GameCollection
{
  /**
   * Get the list of games
   */
  public static function getGames() {
    return Game::from('games AS g')
      ->join('game_types AS ty', 'ty.id', '=', 'g.type_id')
      ->join('game_themes AS th', 'th.id', '=', 'g.theme_id')
      ->select(['g.id', 'g.name', 'g.image_url', 'g.number', 'g.time', 'g.type_id', 'g.theme_id', 'ty.name AS type_name', 'th.name AS theme_name'])
      ->get();
  }

  /**
   * Get the list of games (from a given theme)
   */
  public static function getGamesFromThemeId($theme_id) {
    return Game::from('games AS g')
      ->join('game_types AS ty', 'ty.id', '=', 'g.type_id')
      ->join('game_themes AS th', 'th.id', '=', 'g.theme_id')
      ->select(['g.id', 'g.name', 'g.image_url', 'g.number', 'g.time', 'g.type_id', 'g.theme_id', 'ty.name AS type_name', 'th.name AS theme_name'])
      ->where('th.id', $theme_id)
      ->get();
  }

  /**
   * Get game information from a given user and game ids
   * Best score, best time, best row, goals and achieved one
   */
  public static function getGameInfoFromUserIdGameId($user_id, $game_id) {
    // TODO: To be implemented
  }
}