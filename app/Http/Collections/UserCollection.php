<?php

/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 19/06/17
 * Time: 00:23
 */

namespace App\Http\Collections;
use App\User;

class UserCollection
{
  /**
   * Display the list of favorite recipes
   */
  public static function login($username, $password)
  {
    $hashed_password = md5($password);
    $req = User::where('email', $username)->where('password', $hashed_password)->first();
    if (count($req) > 0) {
        $req->api_token = str_random(60);
        $req->save();
    }
    return $req;
  }

  public static function signup($request)
  {
    $hashed_password = md5($request->password);

    $data = new User;
    $data->firstname = $request->firstname;
    $data->name = $request->name;
    $data->sexe = $request->sexe;
    $data->age = $request->age;
    $data->country = $request->country;
    $data->password = $hashed_password;
    $data->email = $request->email;
    $data->api_token = str_random(60);

    $data->save();
    $response = array('id' => $data->id);
    return $response;
  }

  public static function update($request)
  {
    $data = User::where('email', $request->email)->first();

    $data->firstname = $request->firstname;
    $data->name = $request->name;
    $data->age = $request->age;
    $data->country = $request->country;

    $data->save();
    $response = array('id' => $data->id);
    return $response;
  }

  public static function show($user_id) {
    return User::where('id', $user_id)->with('recipes')->get()->first();
  }
}
