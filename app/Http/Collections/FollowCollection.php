<?php

namespace App\Http\Collections;
use Illuminate\Http\Request;
use App\Follow;

class FollowCollection
{
  public static function getFollowers($user_id)
  {
    return Follow::from('follows AS f')
      ->join('users AS u', 'f.user_id', '=', 'u.id')
      ->where('f.user_id', $user_id)
      ->select(['u.id AS follower_id', 'f.user_id', 'u.name AS user.name',
                'u.firstname AS user.firstname', 'u.pictureUrl'])
      ->distinct()
      ->get();
  }

  public static function getFollowings($user_id) {
    return Follow::from('follows AS f')
      ->join('users AS u', 'f.user_id', '=', 'u.id')
      ->where('f.follower_id', $user_id)
      ->select(['u.id AS following_id', 'f.user_id', 'u.name AS user.name',
                'u.firstname AS user.firstname', 'u.pictureUrl'])
      ->distinct()
      ->get();
  }
}
