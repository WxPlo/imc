<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 16/09/17
 * Time: 20:30
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class QuizRecord extends Model
{
  public $timestamps = false;
  public $fillable = ['id', 'best_score', 'best_time', 'best_row'];
  public $table = 'quiz_records';

  public function game() {
    return $this->belongsTo('App\Game');
  }

  public function user() {
    return $this->belongsTo('App\User');
  }
}