<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
  public $timestamps = false;

  public function follower() {
    return $this->belongsTo('app\User');
  }

  public function user() {
    return $this->belongsTo('app\User');
  }
}
