<?php
/**
 * Created by PhpStorm.
 * User: kelly
 * Date: 09/09/17
 * Time: 00:22
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
  public $timestamps = false;

  public $fillable = ['name', 'image_url', 'number', 'time'];

  public function type() {
    return $this->belongsTo('App\GameType');
  }

  public function theme() {
    return $this->belongsTo('App\GameTheme');
  }
}