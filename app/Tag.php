<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
  public $timestamps = false;
  protected $primaryKey = 'id';

  protected $fillable = [
      'name'
  ];

  public function recipes() {
    return $this->belongsToMany('app\Recipe');
  }
}