<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
  public $timestamps = false;

  protected $fillable = [
      'order', 'description', 'difficulty', 'duration', 'timer_duration', 'mini_game_id'
  ];

  protected $primary_key = 'id';

  public function recipe() {
    return $this->belongsTo('app\Recipe');
  }

  public function ingredients() {
    return $this->belongsToMany('App\Ingredient');
  }

  public function mini_game() {
    return $this->belongsTo('App\MiniGame');
  }
}