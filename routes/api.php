<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/test',  'Api\RecipeController@summerRecipes');
// Auth
Route::post('/signup',  'Api\UserController@signUp');
Route::post('/log',  'Api\UserController@login');

//ImageViewer
Route::post('/addImageViewer',  'Api\ImageViewerController@store');
Route::get('/imageViewer/{recipeId}',  'Api\ImageViewerController@getByRecipe');
Route::delete('/deleteImageViewer/{id}', 'Api\ImageViewerController@destroy');

//ProfilPicture
Route::post('/profile/updatePicture',  'Api\ProfileController@updateProfilePicture');

Route::group(['middleware' => ['auth:api']], function()
{

	// Account
	Route::patch('/profile/{id}',  'Api\ProfileController@editProfile');

	// Follow
	Route::post('/follow',  'Api\FollowController@store');
	Route::get('/follow/followers/{userId}',  'Api\FollowController@showFollowers');
	Route::get('/follow/followings/{userId}',  'Api\FollowController@showFollowings');
	Route::delete('/unfollow/{userId}/{followerId}', 'Api\FollowController@destroy');
	// Comment
	Route::post('/comment',  'Api\CommentController@store');
	Route::get('/comment/{recipeId}',  'Api\CommentController@showAll');
	Route::delete('/uncomment/{commentId}',  'Api\CommentController@destroy');

	// Recipes
	Route::post('/addRecipe',  'Api\RecipeController@store');
	Route::get('/recipes/user/{userId}',  'Api\RecipeController@showFromUserId');
	Route::get('/recipes/tag/{tagName}',  'Api\RecipeController@showFromTagName');
  Route::get('/recipes/id/{id}/ingredients', 'Api\RecipeController@indexIngredientsFromRecipeId');
  Route::get('/recipes/id/{recipeId}',  'Api\RecipeController@show');
  Route::get('/recipes',  'Api\RecipeController@showAll');
	Route::get('/recipes/favorite/user/{userId}',  'Api\RecipeController@indexFavoriteFromUserId');
	Route::patch('/editRecipe/{id}',  'Api\RecipeController@edit');
	Route::get('/ingredients', 'Api\IngredientController@getIngredients');

  // Likes
	Route::get('/likes/{recipeId}', 'Api\LikeController@getLikes');
	Route::get('/like/{recipeId}/{userId}', 'Api\LikeController@getLike');
	Route::post('/addLike', 'Api\LikeController@store');
	Route::delete('/deleteLike/{recipeId}/{userId}', 'Api\LikeController@destroy');

	// Tags
	Route::get('tags', 'Api\TagController@getTags');

	// Messages
	Route::get('/message/{messageId}', 'Api\MessageController@getMessage');
	Route::get('/messages/{userId}', 'Api\MessageController@getUserMessages');
	Route::get('/messages/sent/{userId}', 'Api\MessageController@getUserSentMessages');
	Route::get('/messages/received/{userId}', 'Api\MessageController@getUserReceivedMessages');
	Route::get('/messages/conversations/{userId}', 'Api\MessageController@getConversations');
	Route::get('/messages/{userId}/{user2Id}', 'Api\MessageController@getUsersMessages');
	Route::post('/addMessage', 'Api\MessageController@store');
	Route::delete('/deleteMessage/{messageId}', 'Api\MessageController@destroy');

	// Users
	Route::get('users/id/{id}', 'Api\UserController@show');
  Route::get('users', 'Api\UserController@index');

  // Mini Games
  Route::get('minigames', 'Api\MiniGameController@index');

  // Games (Android app)
  Route::get('/games/{themeId}', 'Api\GameController@indexFromThemeId');
  Route::get('/games', 'Api\GameController@index');

  // Game themes (Android app)
  Route::get('/gamethemes', 'Api\GameThemeController@index');

  Route::get('/quizzes/game/{gameId}/number/{number}', 'Api\QuizController@indexFromGameIdNumber');
  Route::get('/quizzes', 'Api\QuizController@index');
  Route::get('/record/quiz/game/{game_id}/user/{user_id}', 'Api\QuizController@getRecord');
  Route::put('/record/quiz', 'Api\QuizController@updateRecord');
  Route::post('/record/quiz', 'Api\QuizController@storeRecord');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/ss', function () {
    return View::make('greeting');
});
