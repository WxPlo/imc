<?php

use Illuminate\Http\Request;

use App\Recipe;
use App\Comment;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
  return View::make('index');
});

// Route::get('/main', function () {
//       $request = Request::create('/api/recipes', 'GET');
//       $response = Route::dispatch($request)->status();
//       return View::make('greeting')->with('status', $response);
// });

Route::post('/log', 'Web\UserController@login');
Route::get('/log', function () { return View::make('log'); });
Route::get('/logout', 'Web\UserController@logout');

Route::post('/recipe/comment', 'Web\CommentController@store');
Route::get('/recipe/create', 'Web\RecipeController@createView');
Route::post('/recipe/store', ['uses' => 'Web\RecipeController@store', 'as' => 'recipe-creation']);
Route::get('/recipe/{id}/update', 'Web\RecipeController@updateView');
Route::post('/recipe/{id}/update', ['uses' => 'Web\RecipeController@update', 'as' => 'recipe-edition']);
Route::post('/recipe/delete', ['uses' => 'Web\RecipeController@delete', 'as' => 'recipe-deletion']);
Route::post('/recipe/{id}/like', ['uses' => 'Web\LikeController@store', 'as' => 'like']);
Route::post('/recipe/{id}/unlike', ['uses' => 'Web\LikeController@destroy', 'as' => 'unlike']);
Route::get('/recipe/{id}', ['uses' => 'Web\RecipeController@show', 'as' => 'recipe']);

Route::get('/recipes/summer', ['uses' => 'Web\RecipeController@summerRecipes', 'as' => 'summer']);
Route::get('/recipes/winter', ['uses' => 'Web\RecipeController@winterRecipes', 'as' => 'winter']);
Route::get('/recipes/spring', ['uses' => 'Web\RecipeController@springRecipes', 'as' => 'spring']);
Route::get('/recipes/autumn', ['uses' => 'Web\RecipeController@autumnRecipes', 'as' => 'autumn']);
Route::get('/recipes/{id}', ['uses' => 'Web\RecipeController@indexFromUserId', 'as' => 'user-recipes']);
Route::get('/recipes', ['uses' => 'Web\RecipeController@index', 'as' => 'recipes']);

Route::get('/favorites/{user_id}', ['uses' => 'Web\RecipeController@indexFavoriteFromUserId']);

Route::get('/search', ['uses' => 'Web\RecipeController@searchView', 'as' => 'search']);
Route::post('/search', ['uses' => 'Web\RecipeController@search', 'as' => 'search']);

Route::get('/profile/{id}/followers', ['uses' => 'Web\FollowController@showFollowers', 'as' => 'followers']);
Route::get('/profile/{id}/followings', ['uses' => 'Web\FollowController@showFollowings', 'as' => 'followings']);
Route::get('/profile/{id}', ['uses' => 'Web\UserController@show', 'as' => 'profile']);
Route::post('/profile/{id}', ['uses' => 'Web\UserController@updateProfile', 'as' => 'updated']);
Route::get('/404', function () { return View::make('404'); });
Route::get('{url}', function () { return View::make('404'); });

//Route::get('/', 'HomeController@index');
//Route::resource('rrecipes', 'RrecipeController');

Route::group(['middleware' => 'web'], function()
{
  //Route::get('/log',  'UserController@login');
  Auth::routes();

  Route::get('/home', 'HomeController@index');
  Route::post('/test',  'UserController@index');
});
