<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CommentTest extends TestCase
{
  /** @test */
  public function err_no_user_id()
  {
      $response = $this->call('POST', 'recipe/comment', array(
        "recipe_id" => 114,
        "data" => "Good recipe."
      ));
      $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_no_recipe_id()
  {
      $response = $this->call('POST', 'recipe/comment', array(
        "user_id" => 88,
        "data" => "Good recipe."
      ));
      $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_no_data()
  {
      $response = $this->call('POST', 'recipe/comment', array(
        "recipe_id" => 114,
        "user_id" => 88
      ));
      $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_no_existing_recipe()
  {
      $response = $this->call('POST', 'recipe/comment', array(
        "recipe_id" => 0,
        "user_id" => 88,
        "data" => "Good recipe."
      ));
      $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_no_existing_user()
  {
      $response = $this->call('POST', 'recipe/comment', array(
        "recipe_id" => 114,
        "user_id" => 0,
        "data" => "Good recipe."
      ));
      $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function valid()
  {
      $response = $this->call('POST', 'recipe/comment', array(
        "recipe_id" => 114,
        "user_id" => 88,
        "data" => "Good recipe!"
      ));
      $this->assertEquals(302, $response->getStatusCode());
  }
}
