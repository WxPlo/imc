<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SignUpTest extends TestCase
{
  /** @test */
  public function err_age_negative_number()
  {
    $response = $this->call('POST', 'log', array(
      'firstname' => 'John',
      'name'      => 'Doe',
      'sexe'      => 'Male',
      'country'   => 'France',
      'age'       => '-1',
      'email'     => 'johndoe@mail.com',
      'password'  => 'Password!12345',
      'cpassword'  => 'Password!12345',
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_firstname_number()
  {
      $response = $this->call('POST', 'log', array(
        'firstname' => 'John1',
        'name'      => 'Doe',
        'sexe'      => 'Male',
        'country'   => 'France',
        'age'       => '26',
        'email'     => 'johndoemail.com',
        'password'  => 'Password!12345',
        'cpassword'  => 'Password!12345',
      ));
      $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_firstname_space()
  {
      $response = $this->call('POST', 'log', array(
        'firstname' => 'John Lala',
        'name'      => 'Doe',
        'sexe'      => 'Male',
        'country'   => 'France',
        'age'       => '26',
        'email'     => 'johndoemail.com',
        'password'  => 'Password!12345',
        'cpassword'  => 'Password!12345',
      ));
      $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_firstname_special_char()
  {
      $response = $this->call('POST', 'log', array(
        'firstname' => 'John!',
        'name'      => 'Doe',
        'sexe'      => 'Male',
        'country'   => 'France',
        'age'       => '26',
        'email'     => 'johndoemail.com',
        'password'  => 'Password!12345',
        'cpassword'  => 'Password!12345',
      ));
      $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_name_number()
  {
      $response = $this->call('POST', 'log', array(
        'firstname' => 'John',
        'name'      => 'Doe1',
        'sexe'      => 'Male',
        'country'   => 'France',
        'age'       => '26',
        'email'     => 'johndoemail.com',
        'password'  => 'Password!12345',
        'cpassword'  => 'Password!12345',
      ));
      $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_name_space()
  {
      $response = $this->call('POST', 'log', array(
        'firstname' => 'John',
        'name'      => 'Doe Hey',
        'sexe'      => 'Male',
        'country'   => 'France',
        'age'       => '26',
        'email'     => 'johndoemail.com',
        'password'  => 'Password!12345',
        'cpassword'  => 'Password!12345',
      ));
      $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_name_special_char()
  {
      $response = $this->call('POST', 'log', array(
        'firstname' => 'John',
        'name'      => 'Doe!',
        'sexe'      => 'Male',
        'country'   => 'France',
        'age'       => '26',
        'email'     => 'johndoemail.com',
        'password'  => 'Password!12345',
        'cpassword'  => 'Password!12345',
      ));
      $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_gender_not_male_female()
  {
      $response = $this->call('POST', 'log', array(
        'firstname' => 'John',
        'name'      => 'Doe',
        'sexe'      => 'random',
        'country'   => 'France',
        'age'       => '26',
        'email'     => 'johndoemail.com',
        'password'  => 'Password!12345',
        'cpassword'  => 'Password!12345',
      ));
      $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_email_no_at()
  {
      $response = $this->call('POST', 'log', array(
        'firstname' => 'John',
        'name'      => 'Doe',
        'sexe'      => 'Male',
        'country'   => 'France',
        'age'       => '26',
        'email'     => 'johndoemail.com',
        'password'  => 'Password!12345',
        'cpassword'  => 'Password!12345',
      ));
      $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_email_extension_more_than_4()
  {
    $response = $this->call('POST', 'log', array(
      'firstname' => 'John',
      'name'      => 'Doe',
      'sexe'      => 'Male',
      'country'   => 'France',
      'age'       => '26',
      'email'     => 'johndoe@mail.abcde',
      'password'  => 'Password!12345',
      'cpassword'  => 'Password!12345',
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_email_extension_less_than_2()
  {
    $response = $this->call('POST', 'log', array(
      'firstname' => 'John',
      'name'      => 'Doe',
      'sexe'      => 'Male',
      'country'   => 'France',
      'age'       => '26',
      'email'     => 'johndoe@mail.a',
      'password'  => 'Password!12345',
      'cpassword'  => 'Password!12345',
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_email_no_extension()
  {
    $response = $this->call('POST', 'log', array(
      'firstname' => 'John',
      'name'      => 'Doe',
      'sexe'      => 'Male',
      'country'   => 'France',
      'age'       => '26',
      'email'     => 'johndoe@mail',
      'password'  => 'Password!12345',
      'cpassword'  => 'Password!12345',
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_email_no_extension_after_dot()
  {
    $response = $this->call('POST', 'log', array(
      'firstname' => 'John',
      'name'      => 'Doe',
      'sexe'      => 'Male',
      'country'   => 'France',
      'age'       => '26',
      'email'     => 'johndoe@mail.',
      'password'  => 'Password!12345',
      'cpassword'  => 'Password!12345',
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  public function err_email_password_less_than_8()
  {
    $response = $this->call('POST', 'log', array(
      'firstname' => 'John',
      'name'      => 'Doe',
      'sexe'      => 'Male',
      'country'   => 'France',
      'age'       => '26',
      'email'     => 'johndoe@mail.com',
      'password'  => 'AaBb1%@',
      'cpassword'  => 'AaBb1%@',
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_email_password_more_than_15()
  {
    $response = $this->call('POST', 'log', array(
      'firstname' => 'John',
      'name'      => 'Doe',
      'sexe'      => 'Male',
      'country'   => 'France',
      'age'       => '26',
      'email'     => 'johndoe@mail.com',
      'password'  => 'Password!1234567',
      'cpassword'  => 'Password!1234567',
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_password_no_uppercase()
  {
    $response = $this->call('POST', 'log', array(
      'firstname' => 'John',
      'name'      => 'Doe',
      'sexe'      => 'Male',
      'country'   => 'France',
      'age'       => '26',
      'email'     => 'johndoe@mail.com',
      'password'  => 'password!12345',
      'cpassword'  => 'password!12345',
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_password_no_lowercase()
  {
    $response = $this->call('POST', 'log', array(
      'firstname' => 'John',
      'name'      => 'Doe',
      'sexe'      => 'Male',
      'country'   => 'France',
      'age'       => '26',
      'email'     => 'johndoe@mail.com',
      'password'  => 'PASSWORD!12345',
      'cpassword'  => 'PASSWORD!12345',
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_password_no_number()
  {
    $response = $this->call('POST', 'log', array(
      'firstname' => 'John',
      'name'      => 'Doe',
      'sexe'      => 'Male',
      'country'   => 'France',
      'age'       => '26',
      'email'     => 'johndoe@mail.com',
      'password'  => 'Password!Passw',
      'cpassword'  => 'Password!Passw',
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_password_no_special_chars()
  {
    $response = $this->call('POST', 'log', array(
      'firstname' => 'John',
      'name'      => 'Doe',
      'sexe'      => 'Male',
      'country'   => 'France',
      'age'       => '26',
      'email'     => 'johndoe@mail.com',
      'password'  => 'Password123456',
      'cpassword'  => 'Password123456',
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_passwords_different()
  {
    $response = $this->call('POST', 'log', array(
      'firstname' => 'John',
      'name'      => 'Doe',
      'sexe'      => 'Male',
      'country'   => 'France',
      'age'       => '26',
      'email'     => 'johndoe@mail.com',
      'password'  => 'Password!12345',
      'cpassword'  => 'Password!12346',
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_existing_account()
  {
    $response = $this->call('POST', 'log', array(
      'firstname' => 'John',
      'name'      => 'Doe',
      'sexe'      => 'Male',
      'country'   => 'France',
      'age'       => '26',
      'email'     => 'johndoe@mail.com',
      'password'  => 'Password!12345',
      'cpassword'  => 'Password!12345',
    ));
    $this->assertEquals(500, $response->getStatusCode());
  }
}
