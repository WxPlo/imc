<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FavoritesTest extends TestCase
{
  /** @test */
  public function valid()
  {
      $response = $this->call('GET', 'favorites/114', array());
      $this->assertEquals(200, $response->getStatusCode());
  }
}
