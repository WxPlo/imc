<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RecipeDeletionTest extends TestCase
{
  /** @test */
  public function err_empty_request()
  {
    $response = $this->call('POST', '/recipe/delete', array());
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_recipe_id_user_id_missing()
  {
    $response = $this->call('POST', '/recipe/delete', array());
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_recipe_id_missing()
  {
    $response = $this->call('POST', '/recipe/delete', array(
      'user_id'            => '85'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_user_id_missing()
  {
    $response = $this->call('POST', '/recipe/delete', array(
      'recipe_id'            => '150'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_user_not_author_nor_admin()
  {
    $response = $this->call('POST', '/recipe/delete', array(
      'user_id'            => '88',
      'recipe_id'          => '150'
    ));
    $this->assertEquals(404, $response->getStatusCode());
  }

  /** @test */
  public function err_no_existing_recipe()
  {
    $response = $this->call('POST', '/recipe/delete', array(
      'user_id'            => '85',
      'recipe_id'          => '0'
    ));
    $this->assertEquals(404, $response->getStatusCode());
  }
}
