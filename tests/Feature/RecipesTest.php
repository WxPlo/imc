<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RecipesTest extends TestCase
{
  /** @test */
  public function err_not_existing_recipe()
  {
      $response = $this->call('GET', 'recipe/0');
      $this->assertEquals(404, $response->getStatusCode());
  }

  /** @test */
  public function existing_recipe()
  {
      $response = $this->call('GET', 'recipe/114');
      $this->assertEquals(200, $response->getStatusCode());
  }
}
