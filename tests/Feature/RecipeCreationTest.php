<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RecipeCreationTest extends TestCase
{
  /** @test */
  public function err_name_missing()
  {
    $response = $this->call('POST', 'recipe/store', array(
      'user_id'       => '84',
      'creation_date' => '2017-07-05',
      'description'   => 'A delicious meal with chicken and rice, from Taiwan.',
      'steps'         => array()
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_user_id_missing()
  {
    $response = $this->call('POST', 'recipe/store', array(
      'name'          => 'Chicken and Rice',
      'creation_date' => '2017-07-05',
      'description'   => 'A delicious meal with chicken and rice, from Taiwan.',
      'steps'         => array()
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_creation_date_missing()
  {
    $response = $this->call('POST', 'recipe/store', array(
      'name'          => 'Chicken and Rice',
      'user_id'       => '84',
      'description'   => 'A delicious meal with chicken and rice, from Taiwan.',
      'steps'         => array()
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_description_missing()
  {
    $response = $this->call('POST', 'recipe/store', array(
      'name'          => 'Chicken and Rice',
      'user_id'       => '84',
      'creation_date' => '2017-07-05',
      'steps'         => array()
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_steps_missing()
  {
    $response = $this->call('POST', 'recipe/store', array(
      'name'          => 'Chicken and Rice',
      'user_id'       => '84',
      'creation_date' => '2017-07-05',
      'description'   => 'A delicious meal with chicken and rice, from Taiwan.'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function valid()
  {
    $response = $this->call('POST', 'recipe/store', array(
      'name'          => 'Chicken and Rice',
      'user_id'       => '84',
      'creation_date' => '2017-07-05',
      'description'   => 'A delicious meal with chicken and rice, from Taiwan.',
      'steps'         => array()
    ));

    $this->assertEquals(200, $response->getStatusCode());
  }
}
