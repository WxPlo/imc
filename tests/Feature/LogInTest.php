<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class LogInTest extends TestCase
{
  /** @test */
  public function err_no_email()
  {
    $response = $this->call('POST', 'log', array(
      'password' => 'Password!12345'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_no_password()
  {
      $response = $this->call('POST', 'log', array(
        'username'     => 'johndo@email.com'
      ));
      $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_email_no_at()
  {
      $response = $this->call('POST', 'log', array(
        'username'     => 'johndoemail.com',
        'password'  => 'Password!12345'
      ));
      $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_email_extension_more_than_4()
  {
    $response = $this->call('POST', 'log', array(
      'username'     => 'johndoe@mail.abcde',
      'password'  => 'Password!12345'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_email_extension_less_than_2()
  {
    $response = $this->call('POST', 'log', array(
      'username'     => 'johndoe@mail.a',
      'password'  => 'Password!12345'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_email_no_extension()
  {
    $response = $this->call('POST', 'log', array(
      'username'     => 'johndoe@mail',
      'password'  => 'Password!12345'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_email_no_extension_after_dot()
  {
    $response = $this->call('POST', 'log', array(
      'username'     => 'johndoe@mail.',
      'password'  => 'Password!12345'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  public function err_email_password_less_than_8()
  {
    $response = $this->call('POST', 'log', array(
      'username'     => 'johndoe@mail.com',
      'password'  => 'AaBb1%@'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_email_password_more_than_15()
  {
    $response = $this->call('POST', 'log', array(
      'username'     => 'johndoe@mail.com',
      'password'  => 'Password!1234567'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_password_no_uppercase()
  {
    $response = $this->call('POST', 'log', array(
      'username'     => 'johndoe@mail.com',
      'password'    => 'password!12345'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_password_no_lowercase()
  {
    $response = $this->call('POST', 'log', array(
      'username'     => 'johndoe@mail.com',
      'password'  => 'PASSWORD!12345'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_password_no_number()
  {
    $response = $this->call('POST', 'log', array(
      'username'     => 'johndoe@mail.com',
      'password'  => 'Password!Passw'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_password_no_special_chars()
  {
    $response = $this->call('POST', 'log', array(
      'username'     => 'johndoe@mail.com',
      'password'  => 'Password123456'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_not_existing_account()
  {
    $response = $this->call('POST', 'log', array(
      'username'     => 'johndoe_not_existing@mail.com',
      'password'  => 'Password!12345'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function err_wrong_password()
  {
    $response = $this->call('POST', 'log', array(
      'username'     => 'johndoe@mail.com',
      'password'  => 'Password123456'
    ));
    $this->assertEquals(400, $response->getStatusCode());
  }

  /** @test */
  public function valid()
  {
    $response = $this->call('POST', 'log', array(
      'username'     => 'johndoe@mail.com',
      'password'  => 'Password!12345'
    ));
    $this->assertEquals(302, $response->getStatusCode());
  }
}
