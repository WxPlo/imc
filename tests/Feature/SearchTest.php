<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SearchTest extends TestCase
{
  /** @test */
  public function valid_ingredient()
  {
      $response = $this->call('POST', 'search', array(
        'ingredient' => 1
      ));
      $this->assertEquals(200, $response->getStatusCode());
  }

  /** @test */
  public function valid_tag()
  {
      $response = $this->call('POST', 'search', array(
        'tag' => 1));
      $this->assertEquals(200, $response->getStatusCode());
  }

  /** @test */
  public function valid_name_tag()
  {
      $response = $this->call('POST', 'search', array(
        'name' => 'tomate',
        'tag' => 1));
      $this->assertEquals(200, $response->getStatusCode());
  }

  /** @test */
  public function valid_tag_ingredient()
  {
      $response = $this->call('POST', 'search', array(
        'ingredient' => 1,
        'tag' => 1));
      $this->assertEquals(200, $response->getStatusCode());
  }

  /** @test */
  public function valid_name_ingredient_tag()
  {
      $response = $this->call('POST', 'search', array(
        'name' => 'tomate',
        'ingredient' => 1,
        'tag' => 1));
      $this->assertEquals(200, $response->getStatusCode());
  }

}
